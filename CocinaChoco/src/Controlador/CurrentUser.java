/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author oscar
 */
public class CurrentUser {
    private String idEmpleado, nombre; 
    private int permisos;
    private Conexion conexion = new Conexion();
    
    public CurrentUser(String idEmpleado, String nombre, int permisos) {
        this.idEmpleado = idEmpleado;
        this.nombre = nombre;
        this.permisos = permisos;
    }

    public String getIdEmpleado() {
        return idEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public int getPermisos() {
        return permisos;
    }
    
    
    
    public void asignarUsuario(String idEmpleado){
        try {
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "SELECT idEmpleado, LEFT(nombre, locate(' ', nombre) - 1) as Nombre, permisos from empleados where idEmpleado = '" + idEmpleado + "';"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            if (rs.next()) {
                this.idEmpleado = rs.getString(1);
                this.nombre = rs.getString(2);
                this.permisos = rs.getInt(3);
            } else {
                System.out.println("No se encontraron resultados");
            }

            rs.close();
            s.close();
            conexion.disconnectDatabase(con);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
