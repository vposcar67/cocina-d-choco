/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Vista.inicioAdmin;
import Vista.inicioEmpleado;
import javax.swing.JFrame;

public class SalirInicio {
    private CurrentUser CU;
    private javax.swing.JFrame inicio;

    public SalirInicio(CurrentUser CU, JFrame inicio) {
        this.CU = CU;
        this.inicio = inicio;
    }
    
    public void Salir(){
        if(CU.getPermisos() >= 2 ){
            inicioAdmin inicio = new inicioAdmin(CU.getNombre());
            controladorInicioAdmin cIniA = new controladorInicioAdmin(inicio,CU);
            if(CU.getPermisos() >= 3){
                inicio.lblRango.setText("MAESTRO");
                inicio.SetImageLabel(inicio.lblPenguin, "src/imagenes/pingui.png");
            }
            inicio.setVisible(true);
        } else{
            inicioEmpleado inicio = new inicioEmpleado(CU.getNombre());
            controladorInicioEmpleado cIniE = new controladorInicioEmpleado(inicio,CU);
            inicio.setVisible(true);
        }
        inicio.dispose();
    }
}
