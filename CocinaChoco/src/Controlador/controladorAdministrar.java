
package Controlador;

import Modelo.modeloEmpleados;
import Modelo.modeloProveedores;
import Modelo.modeloRecetas;
import Vista.Administrar;
import Vista.empleados;
import Vista.proveedores;
import Vista.recetas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorAdministrar implements ActionListener{
    
    private CurrentUser CU;
    private Administrar admin;

    public controladorAdministrar(CurrentUser CU, Administrar admin) {
        this.CU = CU;
        this.admin = admin;
        this.admin.btnEmpleados.addActionListener(this);
        this.admin.btnProvedores.addActionListener(this);
        this.admin.btnRecetas.addActionListener(this);
        this.admin.btnSalir.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(admin.btnSalir == e.getSource()){
            SalirInicio SI = new SalirInicio(CU, admin);
            SI.Salir();
        }
        if(admin.btnEmpleados == e.getSource()){
            empleados empleados = new empleados();
            modeloEmpleados mEmp = new modeloEmpleados();
            controladorEmpleados cEmp = new controladorEmpleados(CU,empleados,mEmp);
            empleados.setVisible(true);
            admin.dispose();
        }
        if(admin.btnProvedores == e.getSource()){
            proveedores prov = new proveedores();
            modeloProveedores mPro = new modeloProveedores();
            controladorProveedores cPro = new controladorProveedores(CU,prov,mPro);
            prov.setVisible(true);
            admin.dispose();
        }
        if(admin.btnRecetas == e.getSource()){
            recetas recetas = new recetas();
            modeloRecetas mRec = new modeloRecetas();
            controladorRecetas cRec = new controladorRecetas(CU,recetas,mRec);
            recetas.setVisible(true);
            admin.dispose();
        }
    }
    
    
}
