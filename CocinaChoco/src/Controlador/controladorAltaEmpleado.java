/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloEmpleados;
import Vista.altaEmpleado;
import Vista.empleados;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorAltaEmpleado implements ActionListener{
    private modeloEmpleados mEmp;
    private altaEmpleado aEmp;
    private empleados emp;

    public controladorAltaEmpleado(modeloEmpleados mEmp, altaEmpleado aEmp, empleados emp) {
        this.mEmp = mEmp;
        this.aEmp = aEmp;
        this.emp = emp;
        aEmp.btnGuardar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(mEmp.añadir(aEmp)){
            mEmp.consultaR(emp.jtEmpleados);
            aEmp.dispose();
        }
    }
}
