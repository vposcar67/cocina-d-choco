/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloInventario;
import Vista.Inventario;
import Vista.anadirInventario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorAnadirInventario implements ActionListener{
    private modeloInventario mInv;
    private anadirInventario aI;
    private Inventario inv;
    
    public controladorAnadirInventario(modeloInventario mInv, anadirInventario aI,Inventario inv){
        this.mInv = mInv;
        this.aI = aI;
        this.inv = inv;
        this.aI.btnAñadir.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(mInv.añadir(aI)){
            aI.dispose();
            mInv.consultaR(inv.tbInventario);
        }
    }
}
