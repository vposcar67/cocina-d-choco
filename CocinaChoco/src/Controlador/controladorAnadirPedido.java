/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vista.pedidos;
import Vista.realizarPedido;
import Modelo.modeloPedidos;
import Modelo.modeloInventario;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author A
 */
public class controladorAnadirPedido implements ActionListener, MouseListener, KeyListener{
    private pedidos ped;
    private realizarPedido rPed;
    private modeloPedidos mPed;
    private modeloInventario mInv;
    private CurrentUser CU;
    public String[] insumo = new String[3];
    public float costoInsumo;
    private String idInsumo;
    private int filaEliminar;
    
    public controladorAnadirPedido(pedidos ped, realizarPedido rPed, modeloPedidos mPed, CurrentUser CU) {
        this.CU = CU;
        this.ped = ped;
        this.rPed = rPed;
        this.mPed = mPed;
        this.rPed.tbBusquedaIngrediente.addMouseListener(this);
        this.rPed.txfBarraIngrediente.addKeyListener(this);
        this.rPed.tbIngredientes.addMouseListener(this);
        this.rPed.btnRealizarPedido.addActionListener(this);
        this.rPed.btnAgregarIngrediente.addActionListener(this);
        this.rPed.btnEliminarIngrediente.addActionListener(this);
        this.mPed.Cargar(rPed, CU);
        mInv = new modeloInventario();
    }

    private Float suma(){
        int contar = rPed.tbIngredientes.getRowCount();
        float suma = 0.0f   ;
        for(int i = 0; i < contar; i++){
            suma = suma + Float.parseFloat(rPed.tbIngredientes.getValueAt(i,3).toString());
        }
        return suma;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(rPed.btnRealizarPedido == e.getSource()){
            if(mPed.rPedido(rPed,CU)){
                mPed.consultaR(ped.tablaPedidos);
                rPed.dispose();
            } 
        }
        if(rPed.btnAgregarIngrediente == e.getSource()){
            Object[] fila = new Object[4];
            fila[0] = insumo[0];
            fila[1] = insumo[1];
            fila[2] = Float.parseFloat(rPed.txfCantidad.getText());
            fila[3] = costoInsumo * Float.parseFloat(rPed.txfCantidad.getText());
            rPed.modelo.addRow(fila);
            rPed.txfCostoTotal.setText(""+suma());
            rPed.txfCantidad.setText("");
        }
        if(rPed.btnEliminarIngrediente == e.getSource()){
            rPed.modelo.removeRow(filaEliminar);
            rPed.txfCostoTotal.setText(""+suma());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(rPed.tbBusquedaIngrediente == e.getSource()){
            int  fila = rPed.tbBusquedaIngrediente.rowAtPoint(e.getPoint());
            if (fila > -1){
                idInsumo = String.valueOf(rPed.tbBusquedaIngrediente.getValueAt(fila, 0));
                mPed.getInsumo(this, idInsumo);
                rPed.lblUnidades.setText(insumo[2]);
            }
            if (idInsumo != null){
                rPed.btnAgregarIngrediente.setEnabled(true);
            }
        } else
        if(rPed.tbIngredientes == e.getSource()){
            filaEliminar = rPed.tbIngredientes.rowAtPoint(e.getPoint());
            rPed.btnEliminarIngrediente.setEnabled(true);
        }
        else{
            rPed.btnEliminarIngrediente.setEnabled(false);
            rPed.btnAgregarIngrediente.setEnabled(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
       
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(rPed.txfBarraIngrediente == e.getSource()){
           mPed.buscar(rPed.txfBarraIngrediente,rPed.tbBusquedaIngrediente);
       }
    }
    
}
