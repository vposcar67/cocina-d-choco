/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloProveedores;
import Vista.anadirProveedor;
import Vista.proveedores;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class controladorAnadirProveedor implements ActionListener {
    private proveedores prov;
    private anadirProveedor aProv;
    private modeloProveedores mProv;

    public controladorAnadirProveedor(proveedores prov, anadirProveedor aProv, modeloProveedores mProv) {
        this.prov = prov;
        this.aProv = aProv;
        this.mProv = mProv;
        this.aProv.btnAñadir.addActionListener(this);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(mProv.añadir(aProv)){
            aProv.dispose();
            mProv.consultaR(prov.tablaProveedores);
        }
    }
    
}
