/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloRecetas;
import Vista.anadirReceta;
import Vista.recetas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class controladorAnadirReceta implements ActionListener, MouseListener,KeyListener{

    private anadirReceta aRec;
    private modeloRecetas mRec;
    private recetas recetas;
    public String[] insumo = new String[3];
    public float costoInsumo;
    private String idInsumo;
    private int filaEliminar;
    
    public controladorAnadirReceta(anadirReceta aRec, modeloRecetas mRec, recetas recetas) {
        this.aRec = aRec;
        this.mRec = mRec;
        this.recetas = recetas;
        this.aRec.btnAñadir.addActionListener(this);
        this.aRec.btnEliminarIngrediente.addActionListener(this);
        this.aRec.btnAgregarIngrediente.addActionListener(this);
        this.aRec.tbBusquedaIngrediente.addMouseListener(this);
        this.aRec.tbIngredientes.addMouseListener(this);
        this.aRec.txfBarraIngrediente.addKeyListener(this);
    }
    
    private Float suma(){
        int contar = aRec.tbIngredientes.getRowCount();
        float suma = 0.0f   ;
        for(int i = 0; i < contar; i++){
            suma = suma + Float.parseFloat(aRec.tbIngredientes.getValueAt(i,3).toString());
        }
        return suma;
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        mRec.buscar(aRec.txfBarraIngrediente,aRec.tbBusquedaIngrediente);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(aRec.btnAñadir == e.getSource()){
            if(mRec.añadirReceta(aRec)){
                mRec.consultaR(recetas.tablaRecetas);
                aRec.dispose();
            }
        }
        if(aRec.btnAgregarIngrediente == e.getSource()){
            Object[] fila = new Object[4];
            fila[0] = insumo[0];
            fila[1] = insumo[1];
            fila[2] = Float.parseFloat(aRec.txfCantidad.getText());
            fila[3] = costoInsumo * Float.parseFloat(aRec.txfCantidad.getText());
            aRec.modelo.addRow(fila);
            aRec.txfCostoTotal.setText(""+suma());
            aRec.txfCantidad.setText("");
        }
        if(aRec.btnEliminarIngrediente == e.getSource()){
            aRec.modelo.removeRow(filaEliminar);
            aRec.txfCostoTotal.setText(""+suma());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(aRec.tbBusquedaIngrediente == e.getSource()){
            int  fila = aRec.tbBusquedaIngrediente.rowAtPoint(e.getPoint());
            if (fila > -1){
                idInsumo = String.valueOf(aRec.tbBusquedaIngrediente.getValueAt(fila, 0));
                mRec.getInsumo(this, idInsumo);
                aRec.lblUnidades.setText(insumo[2]);
            }
            if (idInsumo != null){
                aRec.btnAgregarIngrediente.setEnabled(true);
            }
        } else
        if(aRec.tbIngredientes == e.getSource()){
            filaEliminar = aRec.tbIngredientes.rowAtPoint(e.getPoint());
            aRec.btnEliminarIngrediente.setEnabled(true);
        }
        else{
            aRec.btnEliminarIngrediente.setEnabled(false);
            aRec.btnAgregarIngrediente.setEnabled(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
       
    }
    
}
