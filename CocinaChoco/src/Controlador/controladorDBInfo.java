package Controlador;

import Modelo.ConexionPrueba;
import Vista.DBInfo;
import Modelo.LoadConnectionInfo;
import Modelo.SaveConnectionInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class controladorDBInfo implements ActionListener {
    
    private DBInfo dbi;

    public controladorDBInfo(DBInfo dbi) {
        this.dbi = dbi;
        this.dbi.btnPrueba.addActionListener(this);
    }
    
    public void abrirDBI(){
        LoadConnectionInfo LCI = new LoadConnectionInfo();
        LCI.CargarinfoConexion();
        this.dbi.txfHost.setText(LCI.getHost());
        this.dbi.txfPort.setText(LCI.getPort());
        this.dbi.txfUser.setText(LCI.getUsername());
        this.dbi.txfPass.setText(LCI.getPassword());
        this.dbi.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(dbi.btnPrueba == e.getSource()) {
            SaveConnectionInfo SCI = new SaveConnectionInfo(this.dbi.txfHost.getText(),this.dbi.txfPort.getText(),this.dbi.txfUser.getText(),this.dbi.txfPass.getText());
            SCI.GuardarInfoConexion();
            ConexionPrueba con = new ConexionPrueba();
            try {
                con.connectToDatabase();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this.dbi, "¡Conexión establecida!\n Reinicie el programa. ");
                dbi.dispose();
            }
        }
    }
    
    
}
