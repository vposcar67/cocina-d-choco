/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloEmpleados;
import Vista.eliminarEmpleado;
import Vista.empleados;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorEliminarEmpleado implements ActionListener {

    private eliminarEmpleado eEmp;
    private modeloEmpleados mEmp;
    private empleados empleados;

    public controladorEliminarEmpleado(eliminarEmpleado eEmp, modeloEmpleados mEmp, empleados empleados) {
        this.eEmp = eEmp;
        this.mEmp = mEmp;
        this.empleados = empleados;
        this.eEmp.btnEliminar.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(mEmp.eliminar(eEmp)){
            mEmp.consultaR(empleados.jtEmpleados);
            eEmp.dispose();
        }
    }
    
}
