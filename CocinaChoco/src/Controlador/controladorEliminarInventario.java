/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloInventario;
import Vista.Inventario;
import Vista.eliminarInventario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class controladorEliminarInventario implements ActionListener{
    private modeloInventario mInv;
    private eliminarInventario eI;
    private Inventario inv;
    
    public controladorEliminarInventario(modeloInventario mInv, eliminarInventario eI,Inventario inv){
        this.mInv = mInv;
        this.eI = eI;
        this.inv = inv;
        this.eI.btnEliminar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(eI.btnEliminar == e.getSource()){   
            if(mInv.eliminar(eI)){
                eI.dispose();
                mInv.consultaR(inv.tbInventario);
            }
        }
    }
}
