/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloProveedores;
import Vista.eliminarProveedor;
import Vista.proveedores;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorEliminarProveedor implements ActionListener {
    private modeloProveedores mProv;
    private eliminarProveedor eP;
    private proveedores prov;
    private String ID;

    public controladorEliminarProveedor(modeloProveedores mProv, eliminarProveedor eP, proveedores prov) {
        this.mProv = mProv;
        this.eP = eP;
        this.prov = prov;
        this.ID = ID;
        this.eP.btnEliminar.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       if(mProv.eliminar(eP)){
           eP.dispose();
           mProv.consultaR(prov.tablaProveedores);
       }
    }
    
}
