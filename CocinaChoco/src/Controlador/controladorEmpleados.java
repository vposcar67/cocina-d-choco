/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Conexion;
import Modelo.modeloEmpleados;
import Vista.Administrar;
import Vista.altaEmpleado;
import Vista.eliminarEmpleado;
import Vista.empleados;
import Vista.modEmpleado;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.*;

/**
 *
 * @author oscar
 */
public class controladorEmpleados implements ActionListener, MouseListener, KeyListener{
    
    private CurrentUser CU;
    private empleados empleados;
    private modeloEmpleados mEmp;
    private String[] seleccion = new String[7];
    private Conexion conexion = new Conexion();

    public controladorEmpleados(CurrentUser CU, empleados empleados, modeloEmpleados mEmp) {
        this.CU = CU;
        this.empleados = empleados;
        this.mEmp = mEmp;
        this.empleados.btnActualizar.addActionListener(this);
        this.empleados.btnAlta.addActionListener(this);
        this.empleados.btnEliminar.addActionListener(this);
        this.empleados.btnModificar.addActionListener(this);
        this.empleados.btnSalir.addActionListener(this);
        this.empleados.jtEmpleados.addMouseListener(this);
        mEmp.consultaR(empleados.jtEmpleados);
        this.empleados.txtID.addKeyListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(empleados.btnActualizar == e.getSource()){
            mEmp.consultaR(empleados.jtEmpleados);
            empleados.txtID.setText("");
        }
        if(empleados.btnAlta == e.getSource()){
            try {
                Connection con = conexion.connectToDatabase();            
                Statement s = con.createStatement();
                String cadenaSQL =  "SELECT nextEMP();";
                ResultSet rs = s.executeQuery(cadenaSQL);

                if (rs.next()) {
                    altaEmpleado aP = new altaEmpleado(rs.getString(1));
                    controladorAltaEmpleado cAI = new controladorAltaEmpleado(mEmp,aP,empleados);
                    aP.setVisible(true);
                } else {
                    System.out.println("No se encontraron resultados");
                }

                rs.close();
                s.close();
                conexion.disconnectDatabase(con);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        if(empleados.btnEliminar == e.getSource()){
            eliminarEmpleado eEmp = new eliminarEmpleado(seleccion[0],seleccion[1],seleccion[2],seleccion[3],seleccion[4],seleccion[5],seleccion[6]);
            controladorEliminarEmpleado cEE = new controladorEliminarEmpleado(eEmp,mEmp,empleados);
            eEmp.setVisible(true);
        }
        if(empleados.btnModificar == e.getSource()){
            modEmpleado modificarEmp = new modEmpleado(seleccion[0],seleccion[1],seleccion[2],seleccion[3],seleccion[4],seleccion[5],seleccion[6]);
            controladorModEmpleado cME = new controladorModEmpleado(mEmp,modificarEmp,empleados);
            modificarEmp.setVisible(true);
        }
        if(empleados.btnSalir == e.getSource()){
            Administrar admin = new Administrar();
            controladorAdministrar cAdmin = new controladorAdministrar(CU,admin);
            admin.setVisible(true);
            empleados.dispose();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(empleados.jtEmpleados == e.getSource()){
            int  fila = empleados.jtEmpleados.rowAtPoint(e.getPoint());
            if (fila > -1){
                seleccion[0] = String.valueOf(empleados.jtEmpleados.getValueAt(fila, 0));
                seleccion[1] = String.valueOf(empleados.jtEmpleados.getValueAt(fila, 1));
                seleccion[2] = String.valueOf(empleados.jtEmpleados.getValueAt(fila, 2));
                seleccion[3] = String.valueOf(empleados.jtEmpleados.getValueAt(fila, 3));
                seleccion[4] = String.valueOf(empleados.jtEmpleados.getValueAt(fila, 4));
                seleccion[5] = String.valueOf(empleados.jtEmpleados.getValueAt(fila, 5));
                seleccion[6] = String.valueOf(empleados.jtEmpleados.getValueAt(fila, 6));
            }
            if (seleccion != null){
                empleados.btnEliminar.setEnabled(true);
                empleados.btnModificar.setEnabled(true);
            }
            
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        mEmp.barraBusqueda(empleados.jtEmpleados, empleados.txtID);
    }
    
    
}
