/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloInventario;
import Modelo.modeloLogin;
import Modelo.modeloPedidos;
import Modelo.modeloPreparaciones;
import Vista.Administrar;
import Vista.Inventario;
import Vista.inicioAdmin;
import Vista.login;
import Vista.pedidos;
import Vista.preparaciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorInicioAdmin implements ActionListener{
    private inicioAdmin iniA;
    private CurrentUser CU;
    
    public controladorInicioAdmin(inicioAdmin iniA,CurrentUser CU){
        this.iniA = iniA;
        this.CU = CU;
        this.iniA.btnSalir.addActionListener(this);
        this.iniA.btnInventario1.addActionListener(this);
        this.iniA.btnPedidos.addActionListener(this);
        this.iniA.btnPreparaciones.addActionListener(this);
        this.iniA.btnAdministrar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(iniA.btnInventario1 == e.getSource()){
            Inventario inv = new Inventario();
            modeloInventario mInv = new modeloInventario();
            controladorInventario cInv = new controladorInventario(CU,inv,mInv);
            inv.setVisible(true);
            iniA.dispose();
        } else
        if(iniA.btnPedidos == e.getSource()){
            pedidos pedidos = new pedidos();
            modeloPedidos mPed = new modeloPedidos();
            controladorPedidos cPed = new controladorPedidos(CU,pedidos,mPed);
            pedidos.setVisible(true);
            iniA.dispose();
        }else
        if(iniA.btnPreparaciones == e.getSource()){
            preparaciones preparaciones = new preparaciones();
            modeloPreparaciones mPre = new modeloPreparaciones();
            controladorPreparaciones cPre = new controladorPreparaciones(CU,mPre,preparaciones);
            preparaciones.setVisible(true);
            iniA.dispose();
        }else 
        if(iniA.btnAdministrar == e.getSource()){
            Administrar admin = new Administrar();
            controladorAdministrar cAdmin = new controladorAdministrar(CU,admin);
            admin.setVisible(true);
            iniA.dispose();
        }else
        if(iniA.btnSalir == e.getSource()){
            login log = new login();
            modeloLogin mlog = new modeloLogin();
            controladorLogin l = new controladorLogin(log,mlog);
            l.LogIn();
            iniA.dispose();
        }
        
    }
    
}