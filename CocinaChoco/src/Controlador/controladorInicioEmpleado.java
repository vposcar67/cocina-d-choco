/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloInventario;
import Modelo.modeloLogin;
import Modelo.modeloPedidos;
import Modelo.modeloPreparaciones;
import Vista.Inventario;
import Vista.inicioEmpleado;
import Vista.login;
import Vista.pedidos;
import Vista.preparaciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorInicioEmpleado implements ActionListener{
    private inicioEmpleado iniE;
    private CurrentUser CU;
    
    public controladorInicioEmpleado(inicioEmpleado iniE,CurrentUser CU){
       this.iniE = iniE;
        this.CU = CU;
        this.iniE.btnSalir.addActionListener(this);
        this.iniE.btnInventario1.addActionListener(this);
        this.iniE.btnPedidos.addActionListener(this);
        this.iniE.btnPreparaciones.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(iniE.btnInventario1 == e.getSource()){
            Inventario inv = new Inventario();
            modeloInventario mInv = new modeloInventario();
            controladorInventario cInv = new controladorInventario(CU,inv,mInv);
            inv.setVisible(true);
            iniE.dispose();
        } else
        if(iniE.btnPedidos == e.getSource()){
            pedidos pedidos = new pedidos();
            modeloPedidos mPed = new modeloPedidos();
            controladorPedidos cPed = new controladorPedidos(CU,pedidos,mPed);
            pedidos.setVisible(true);
            iniE.dispose();
        }else
        if(iniE.btnPreparaciones == e.getSource()){
            preparaciones preparaciones = new preparaciones();
            modeloPreparaciones mPre = new modeloPreparaciones();
            controladorPreparaciones cPre = new controladorPreparaciones(CU,mPre,preparaciones);
            preparaciones.setVisible(true);
            iniE.dispose();
        }else
        if(iniE.btnSalir == e.getSource()){
            login log = new login();
            modeloLogin mlog = new modeloLogin();
            controladorLogin l = new controladorLogin(log,mlog);
            l.LogIn();
            iniE.dispose();
        }
        
    }
    
}
