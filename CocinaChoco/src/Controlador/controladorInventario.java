/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Conexion;
import Modelo.modeloInventario;
import Vista.Inventario;
import Vista.anadirInventario;
import Vista.eliminarInventario;
import Vista.modInventario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class controladorInventario implements ActionListener, MouseListener, KeyListener{
    
    private CurrentUser CU;
    private Inventario inventario;
    private modeloInventario mInv;
    private Conexion conexion = new Conexion();
    private String[] seleccion = new String[6];


    public controladorInventario(CurrentUser CU, Inventario inventario, modeloInventario mInv) {
        this.CU = CU;
        this.inventario = inventario;
        this.mInv = mInv;
        this.inventario.btnSalir.addActionListener(this);
        this.inventario.btnActualizar.addActionListener(this);
        this.inventario.btnAñadir.addActionListener(this);
        this.inventario.btnEliminar.addActionListener(this);
        this.inventario.btnModificar.addActionListener(this);
        this.inventario.tbInventario.addMouseListener(this);
        this.inventario.txtBarra.addKeyListener(this);
        mInv.consultaR(inventario.tbInventario);
        if(CU.getPermisos() != 2){
            inventario.btnEliminar.setEnabled(false);
            inventario.btnAñadir.setEnabled(false);
            inventario.btnModificar.setEnabled(false);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(inventario.tbInventario == e.getSource()){
            int  fila = inventario.tbInventario.rowAtPoint(e.getPoint());
            if (fila > -1){
                seleccion[0] = String.valueOf(inventario.tbInventario.getValueAt(fila, 0));
                seleccion[1] = String.valueOf(inventario.tbInventario.getValueAt(fila, 1));
                seleccion[2] = String.valueOf(inventario.tbInventario.getValueAt(fila, 2));
                int espacio = seleccion[2].indexOf(" ");
                seleccion[3] = seleccion[2].substring(espacio+1);
                seleccion[2] = seleccion[2].substring(0, espacio);
                seleccion[4] = String.valueOf(inventario.tbInventario.getValueAt(fila, 3));
                seleccion[5] = String.valueOf(inventario.tbInventario.getValueAt(fila, 4));
            }
            if (seleccion != null && CU.getPermisos() >=2){
                inventario.btnEliminar.setEnabled(true);
                inventario.btnModificar.setEnabled(true);
            }
            
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(inventario.btnSalir == e.getSource()) {
            SalirInicio SI = new SalirInicio(CU,inventario);
            SI.Salir();
        } else
        if(inventario.btnActualizar == e.getSource()) {
            mInv.consultaR(inventario.tbInventario);
            inventario.txtBarra.setText("");
        } else
        if(inventario.btnAñadir == e.getSource()) {
            try {
                Connection con = conexion.connectToDatabase();            
                Statement s = con.createStatement();
                String cadenaSQL =  "SELECT nextINS();";
                ResultSet rs = s.executeQuery(cadenaSQL);

                if (rs.next()) {
                    anadirInventario aI = new anadirInventario(rs.getString(1));
                    controladorAnadirInventario cAI = new controladorAnadirInventario(mInv,aI,inventario);
                    aI.setVisible(true);
                } else {
                    System.out.println("No se encontraron resultados");
                }

                rs.close();
                s.close();
                conexion.disconnectDatabase(con);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            
        }else
        if(inventario.btnEliminar == e.getSource()) {
            eliminarInventario eI = new eliminarInventario(seleccion[0], seleccion[1], seleccion[2], seleccion[3], seleccion[4], seleccion[5]);
            controladorEliminarInventario cEI = new controladorEliminarInventario(mInv,eI,inventario);
            eI.setVisible(true);
        }else
        if(inventario.btnModificar == e.getSource()) {
            modInventario mI = new modInventario(seleccion[0], seleccion[1], seleccion[2], seleccion[3], seleccion[4].substring(1), seleccion[5]);
            controladorModInventario cMI = new controladorModInventario(mInv,mI,inventario); 
            mI.setVisible(true);
        }
    }   

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        mInv.barraBusqueda(inventario.tbInventario,inventario.txtBarra);
    }
}
