package Controlador;

import Vista.login;
import Modelo.modeloLogin;
import Vista.DBInfo;
import Vista.inicioAdmin;
import Vista.inicioEmpleado;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;


public class controladorLogin implements ActionListener{
    private login login;
    private CurrentUser CU;
    private modeloLogin mlog;

    public controladorLogin(login login, modeloLogin mlog) {
        this.login = login;
        this.mlog = mlog;
        this.login.btnConfi1.addActionListener(this);
        this.login.btnEntrar.addActionListener(this);
        CU = new CurrentUser("","",0);
    }
    
    public void LogIn(){
        login.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(login.btnConfi1 == e.getSource()) {
            DBInfo dbi = new DBInfo();
            controladorDBInfo CDBI = new controladorDBInfo(dbi);
            CDBI.abrirDBI();
        }
        if(login.btnEntrar == e.getSource()) {
            if(mlog.ingresar(login.txtID.getText(), login.passLogin.getText()) == 1) {
                CU.asignarUsuario(login.txtID.getText());
                SalirInicio SI = new SalirInicio(CU,login);
                SI.Salir();
            } else {
                JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrectos ","Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
      
