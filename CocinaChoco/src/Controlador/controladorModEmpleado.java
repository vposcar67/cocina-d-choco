/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloEmpleados;
import Vista.empleados;
import Vista.modEmpleado;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorModEmpleado implements ActionListener {

    private modeloEmpleados modeloEmpleado;
    private modEmpleado modificarEmpleado;
    private empleados empleados;

    public controladorModEmpleado(modeloEmpleados modeloEmpleado, modEmpleado modificarEmpleado, empleados empleados) {
        this.modeloEmpleado = modeloEmpleado;
        this.modificarEmpleado = modificarEmpleado;
        this.empleados = empleados;
        this.modificarEmpleado.btnModificar.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
       if(modeloEmpleado.modificar(modificarEmpleado)){
           modeloEmpleado.consultaR(empleados.jtEmpleados);
           modificarEmpleado.dispose();
       }
    }
    
}
