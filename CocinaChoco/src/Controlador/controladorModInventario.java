/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloInventario;
import Vista.Inventario;
import Vista.eliminarInventario;
import Vista.modInventario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorModInventario implements ActionListener{
    private modeloInventario mInv;
    private modInventario mI;
    private Inventario inv;
    
    public controladorModInventario(modeloInventario mInv, modInventario mI,Inventario inv){
        this.mInv = mInv;
        this.mI = mI;
        this.inv = inv;
        this.mI.btnModificar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(mI.btnModificar == e.getSource()){   
            if(mInv.modificar(mI)){
                mI.dispose();
                mInv.consultaR(inv.tbInventario);
            }
        }
    }
}
