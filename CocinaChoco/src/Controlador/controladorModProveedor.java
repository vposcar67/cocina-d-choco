/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloProveedores;
import Vista.modProveedor;
import Vista.proveedores;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorModProveedor implements ActionListener{
    private modeloProveedores mProv;
    private proveedores prov;
    private modProveedor modProv;

    public controladorModProveedor(modeloProveedores mProv, proveedores prov, modProveedor modProv) {
        this.mProv = mProv;
        this.prov = prov;
        this.modProv = modProv;
        this.modProv.btnModificar.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(mProv.modificar(modProv)){
           modProv.dispose();
           mProv.consultaR(prov.tablaProveedores);
       }
    }  
}
