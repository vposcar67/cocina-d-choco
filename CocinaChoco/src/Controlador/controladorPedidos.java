/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Conexion;
import Modelo.modeloPedidos;
import Vista.realizarPedido;
import Vista.pedidos;
import Vista.verPedido;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author oscar
 */
public class controladorPedidos implements ActionListener, MouseListener, KeyListener{
    
    private CurrentUser CU;
    private pedidos pedidos;
    private modeloPedidos mPed;
    private Conexion conexion = new Conexion();
    private String[] seleccion = new String[7];
    
    public controladorPedidos(CurrentUser CU, pedidos pedidos, modeloPedidos mPed){
        this.CU = CU;
        this.pedidos = pedidos;
        this.mPed = mPed;
        this.pedidos.btnSalir.addActionListener(this);
        this.pedidos.btnActualizar.addActionListener(this);
        this.pedidos.btnRealizarPedido.addActionListener(this);
        this.pedidos.btnDetalles.addActionListener(this);
        this.pedidos.tablaPedidos.addMouseListener(this);
        mPed.consultaR(pedidos.tablaPedidos);
        this.pedidos.txtID.addKeyListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(pedidos.btnSalir == e.getSource()){
            SalirInicio SI = new SalirInicio(CU,pedidos);
            SI.Salir();
        }
        if(pedidos.btnActualizar == e.getSource()){
            mPed.consultaR(pedidos.tablaPedidos);
            pedidos.txtID.setText("");
        }
        if(pedidos.btnRealizarPedido == e.getSource()){
            try {
                Connection con = conexion.connectToDatabase();            
                Statement s = con.createStatement();
                String cadenaSQL =  "SELECT nextPED();";
                ResultSet rs = s.executeQuery(cadenaSQL);

                if (rs.next()) {
                    realizarPedido rPed = new realizarPedido(rs.getString(1));
                    controladorAnadirPedido cAP = new controladorAnadirPedido(pedidos,rPed,mPed,CU);
                    rPed.setVisible(true);
                } else {
                    System.out.println("No se encontraron resultados");
                }

                rs.close();
                s.close();
                conexion.disconnectDatabase(con);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        if(pedidos.btnDetalles == e.getSource()){
            verPedido vP = new verPedido(seleccion[0],seleccion[1],seleccion[2],seleccion[3],seleccion[4],seleccion[5],seleccion[6],mPed.getModelo(seleccion[0]));
            controladorVerPedido cVPed = new controladorVerPedido(pedidos,vP,mPed,CU);
            if("En proceso".equals(seleccion[3])){
                vP.btnCancelarPedido.setEnabled(true);
                vP.btnRecibirPedido.setEnabled(true);
            }
            vP.setVisible(true);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(pedidos.tablaPedidos == e.getSource()){
            int  fila = pedidos.tablaPedidos.rowAtPoint(e.getPoint());
            if (fila > -1){
                seleccion[0] = String.valueOf(pedidos.tablaPedidos.getValueAt(fila, 0));
                seleccion[1] = String.valueOf(pedidos.tablaPedidos.getValueAt(fila, 1));
                seleccion[2] = String.valueOf(pedidos.tablaPedidos.getValueAt(fila, 2));
                seleccion[3] = String.valueOf(pedidos.tablaPedidos.getValueAt(fila, 3));
                seleccion[4] = String.valueOf(pedidos.tablaPedidos.getValueAt(fila, 4));
                seleccion[5] = String.valueOf(pedidos.tablaPedidos.getValueAt(fila, 5));
                seleccion[6] = String.valueOf(pedidos.tablaPedidos.getValueAt(fila, 6));
            }
            if (seleccion != null){
                pedidos.btnDetalles.setEnabled(true);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
       
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        mPed.barraBusqueda(pedidos.tablaPedidos, pedidos.txtID);
    }
}
