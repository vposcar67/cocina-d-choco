/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Conexion;
import Modelo.modeloPreparaciones;
import Vista.anadirPreparacion;
import Vista.preparaciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.*;

public class controladorPreparaciones implements ActionListener, KeyListener{
     
    private CurrentUser CU;
    private modeloPreparaciones mPre;
    private preparaciones preparaciones;
    private Conexion conexion = new Conexion();

    public controladorPreparaciones(CurrentUser CU, modeloPreparaciones mPre, preparaciones preparaciones) {
        this.CU = CU;
        this.mPre = mPre;
        this.preparaciones = preparaciones;
        this.preparaciones.btnActualizar.addActionListener(this);
        this.preparaciones.btnPreparar.addActionListener(this);
        this.preparaciones.btnSalir.addActionListener(this);
        mPre.consultaR(preparaciones.tablaPreparaciones);
        this.preparaciones.txtID.addKeyListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(preparaciones.btnSalir == e.getSource()){
            SalirInicio SI = new SalirInicio(CU,preparaciones);
            SI.Salir();
        }
        if(preparaciones.btnPreparar == e.getSource()){
            try {
                java.sql.Connection con = conexion.connectToDatabase();            
                Statement s = con.createStatement();
                String cadenaSQL =  "SELECT nextORD();";
                ResultSet rs = s.executeQuery(cadenaSQL);

                if (rs.next()) {
                    anadirPreparacion aPre = new anadirPreparacion(rs.getString(1));
                    controladorPrepararReceta cAI = new controladorPrepararReceta(CU,mPre,preparaciones,aPre);
                    aPre.setVisible(true);
                } else {
                    System.out.println("No se encontraron resultados");
                }

                rs.close();
                s.close();
                conexion.disconnectDatabase(con);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        if(preparaciones.btnActualizar == e.getSource()){
            mPre.consultaR(preparaciones.tablaPreparaciones);
            preparaciones.txtID.setText("");
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        mPre.barraBusqueda(preparaciones.tablaPreparaciones, preparaciones.txtID);
    }
    
}
