/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloPreparaciones;
import Vista.anadirPreparacion;
import Vista.preparaciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorPrepararReceta implements ActionListener{
    
    private modeloPreparaciones mPre;
    private CurrentUser CU;
    private preparaciones preparaciones;
    private anadirPreparacion aPre;

    public controladorPrepararReceta(CurrentUser CU,modeloPreparaciones mPre, preparaciones preparaciones, anadirPreparacion aPre) {
        this.mPre = mPre;
        this.CU = CU;
        this.preparaciones = preparaciones;
        this.aPre = aPre;
        this.aPre.btnPreparar.addActionListener(this);
        this.mPre.Cargar(this.aPre, this.CU);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(aPre.btnPreparar == e.getSource()){
            if(mPre.Preparar(aPre,CU)){
                aPre.dispose();
                mPre.consultaR(preparaciones.tablaPreparaciones);
            }
        }
    }
    
    
    
}
