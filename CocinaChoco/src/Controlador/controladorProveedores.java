
package Controlador;

import Modelo.Conexion;
import Modelo.modeloProveedores;
import Vista.proveedores;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vista.Administrar;
import Vista.anadirProveedor;
import Vista.eliminarProveedor;
import Vista.modProveedor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.*;

public class controladorProveedores implements ActionListener, MouseListener, KeyListener{
    
    private CurrentUser CU;
    private proveedores prov;
    private modeloProveedores mPro;
    private Conexion conexion = new Conexion();
    private String[] seleccion = new String[7];

    public controladorProveedores(CurrentUser CU, proveedores prov, modeloProveedores mPro) {
        this.CU = CU;
        this.prov = prov;
        this.mPro = mPro;
        this.prov.btnActualizar.addActionListener(this);
        this.prov.btnModificar.addActionListener(this);
        this.prov.btnAñadir.addActionListener(this);
        this.prov.btnEliminar.addActionListener(this);
        this.prov.btnSalir.addActionListener(this);
        this.prov.tablaProveedores.addMouseListener(this);
        mPro.consultaR(prov.tablaProveedores);
        this.prov.txtID.addKeyListener(this);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(prov.btnSalir == e.getSource()){
            Administrar admin = new Administrar();
            controladorAdministrar cAdmin = new controladorAdministrar(CU,admin);
            admin.setVisible(true);
            prov.dispose();
        }
        if(prov.btnEliminar == e.getSource()){
            eliminarProveedor eProv = new eliminarProveedor(seleccion[0],seleccion[1],seleccion[2],seleccion[3],seleccion[4],seleccion[5],seleccion[6]);
            controladorEliminarProveedor cEP = new controladorEliminarProveedor(mPro,eProv,prov);
            eProv.setVisible(true);
        }
        if(prov.btnAñadir == e.getSource()){
            try {
                Connection con = conexion.connectToDatabase();            
                Statement s = con.createStatement();
                String cadenaSQL =  "SELECT nextPRO();";
                ResultSet rs = s.executeQuery(cadenaSQL);

                if (rs.next()) {
                    anadirProveedor aP = new anadirProveedor(rs.getString(1));
                    controladorAnadirProveedor cAI = new controladorAnadirProveedor(prov,aP,mPro);
                    aP.setVisible(true);
                } else {
                    System.out.println("No se encontraron resultados");
                }

                rs.close();
                s.close();
                conexion.disconnectDatabase(con);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        if(prov.btnModificar == e.getSource()){
            modProveedor mProv = new modProveedor(seleccion[0],seleccion[1],seleccion[2],seleccion[3],seleccion[4],seleccion[5],seleccion[6]);
            controladorModProveedor cMP = new controladorModProveedor(mPro,prov,mProv);
            mProv.setVisible(true);
        }
        if(prov.btnActualizar == e.getSource()){
            mPro.consultaR(prov.tablaProveedores);
            prov.txtID.setText("");
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(prov.tablaProveedores == e.getSource()){
            int  fila = prov.tablaProveedores.rowAtPoint(e.getPoint());
            if (fila > -1){
                seleccion[0] = String.valueOf(prov.tablaProveedores.getValueAt(fila, 0));
                seleccion[1] = String.valueOf(prov.tablaProveedores.getValueAt(fila, 1));
                seleccion[2] = String.valueOf(prov.tablaProveedores.getValueAt(fila, 2));
                seleccion[3] = String.valueOf(prov.tablaProveedores.getValueAt(fila, 3));
                seleccion[4] = String.valueOf(prov.tablaProveedores.getValueAt(fila, 4));
                seleccion[5] = String.valueOf(prov.tablaProveedores.getValueAt(fila, 5));
                seleccion[6] = String.valueOf(prov.tablaProveedores.getValueAt(fila, 6));
            }
            if (seleccion != null){
                prov.btnEliminar.setEnabled(true);
                prov.btnModificar.setEnabled(true);
            }
            
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        mPro.barraBusqueda(prov.tablaProveedores, prov.txtID);
    }
    
}
