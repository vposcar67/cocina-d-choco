/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Conexion;
import Modelo.modeloRecetas;
import Vista.Administrar;
import Vista.anadirReceta;
import Vista.recetas;
import Vista.verReceta;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.*;

public class controladorRecetas implements ActionListener, MouseListener, KeyListener{
    
    private CurrentUser CU;
    private recetas recetas;
    private modeloRecetas mRec;
    private Conexion conexion = new Conexion();
    private String[] seleccion = new String[5];

    public controladorRecetas(CurrentUser CU, recetas recetas, modeloRecetas mRec) {
        this.CU = CU;
        this.recetas = recetas;
        this.mRec = mRec;
        this.recetas.btnActualizar.addActionListener(this);
        this.recetas.btnAñadir.addActionListener(this);
        this.recetas.btnVer.addActionListener(this);
        this.recetas.btnSalir.addActionListener(this);
        this.recetas.tablaRecetas.addMouseListener(this);
        mRec.consultaR(recetas.tablaRecetas);
        this.recetas.txtID.addKeyListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(recetas.btnActualizar == e.getSource()){
            mRec.consultaR(recetas.tablaRecetas);
            recetas.txtID.setText("");
        }
        if(recetas.btnAñadir == e.getSource()){
            try {
                Connection con = conexion.connectToDatabase();            
                Statement s = con.createStatement();
                String cadenaSQL =  "SELECT nextREC();";
                ResultSet rs = s.executeQuery(cadenaSQL);

                if (rs.next()) {
                    anadirReceta aR = new anadirReceta(rs.getString(1));
                    controladorAnadirReceta cAI = new controladorAnadirReceta(aR,mRec,recetas);
                    aR.setVisible(true);
                } else {
                    System.out.println("No se encontraron resultados");
                }

                rs.close();
                s.close();
                conexion.disconnectDatabase(con);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        if(recetas.btnVer == e.getSource()){
            int espacio = seleccion[2].indexOf(" ");
            verReceta vRec = new verReceta(seleccion[0],seleccion[1],seleccion[2].substring(0, espacio),seleccion[3],seleccion[4],mRec.getModelo(seleccion[0]));
            controladorVerReceta cVR = new controladorVerReceta(vRec,mRec,recetas);
            vRec.setVisible(true);
        }
        if(recetas.btnSalir == e.getSource()){
            Administrar admin = new Administrar();
            controladorAdministrar cAdmin = new controladorAdministrar(CU,admin);
            admin.setVisible(true);
            recetas.dispose();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(recetas.tablaRecetas == e.getSource()){
            int  fila = recetas.tablaRecetas.rowAtPoint(e.getPoint());
            if (fila > -1){
                seleccion[0] = String.valueOf(recetas.tablaRecetas.getValueAt(fila, 0));
                seleccion[1] = String.valueOf(recetas.tablaRecetas.getValueAt(fila, 1));
                seleccion[2] = String.valueOf(recetas.tablaRecetas.getValueAt(fila, 2));
                seleccion[3] = String.valueOf(recetas.tablaRecetas.getValueAt(fila, 3));
                seleccion[4] = String.valueOf(recetas.tablaRecetas.getValueAt(fila, 5));
            }
            if (seleccion != null){
                recetas.btnVer.setEnabled(true);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        mRec.barraBusqueda(recetas.tablaRecetas, recetas.txtID);
    }
    
    
}
