/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloPedidos;
import Vista.pedidos;
import Vista.verPedido;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author oscar
 */
public class controladorVerPedido implements ActionListener {

    private verPedido vP;
    private modeloPedidos mPed;
    private CurrentUser CU;
    private pedidos pedidos;

    public controladorVerPedido(pedidos pedidos,verPedido vP, modeloPedidos mPed, CurrentUser CU) {
        this.vP = vP;
        this.mPed = mPed;
        this.CU = CU;
        this.pedidos = pedidos;
        this.vP.btnCancelarPedido.addActionListener(this);
        this.vP.btnRecibirPedido.addActionListener(this);
        costoaprox();
    }
    
    private Float costoaprox(){
        int contar = vP.tbIngredientes.getRowCount();
        float suma = 0.0f   ;
        for(int i = 0; i < contar; i++){
            suma = suma + Float.parseFloat(vP.tbIngredientes.getValueAt(i,3).toString());
        }
        vP.txfCostoTotal.setText("$ "+suma);
        return suma;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(vP.btnCancelarPedido == e.getSource()){
            System.out.println("Cancelar");
            if(mPed.cancelar(vP)){
                mPed.consultaR(pedidos.tablaPedidos);
                vP.dispose();
            }
        }
        if(vP.btnRecibirPedido == e.getSource()){
            System.out.println("Recibir");
            if(mPed.recibir(vP, CU)){
                mPed.consultaR(pedidos.tablaPedidos);
                vP.dispose();
            }
        }
    }
    
}
