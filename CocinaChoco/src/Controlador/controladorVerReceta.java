/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.modeloRecetas;
import Vista.anadirReceta;
import Vista.recetas;
import Vista.verReceta;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class controladorVerReceta implements ActionListener, MouseListener,KeyListener{

    private verReceta vRec;
    private modeloRecetas mRec;
    private recetas recetas;
    public String[] insumo = new String[3];
    public float costoInsumo;
    private String idInsumo;
    private int filaEliminar;
    
    public controladorVerReceta(verReceta vRec, modeloRecetas mRec, recetas recetas) {
        this.vRec = vRec;
        this.mRec = mRec;
        this.recetas = recetas;
        this.vRec.btnSwitch.addActionListener(this);
        this.vRec.btnModificar.addActionListener(this);
        this.vRec.btnEliminar1.addActionListener(this);
        this.vRec.btnEliminarIngrediente.addActionListener(this);
        this.vRec.btnAgregarIngrediente.addActionListener(this);
        this.vRec.tbBusquedaIngrediente.addMouseListener(this);
        this.vRec.tbIngredientes.addMouseListener(this);
        this.vRec.txfBarraIngrediente.addKeyListener(this);
    }
    
    private Float suma(){
        int contar = vRec.tbIngredientes.getRowCount();
        float suma = 0.0f   ;
        for(int i = 0; i < contar; i++){
            suma = suma + Float.parseFloat(vRec.tbIngredientes.getValueAt(i,3).toString());
        }
        return suma;
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        mRec.buscar(vRec.txfBarraIngrediente,vRec.tbBusquedaIngrediente);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(vRec.btnSwitch == e.getSource()){
            if(vRec.btnEliminar1.isEnabled() == true){
                vRec.btnEliminar1.setEnabled(false);
                vRec.btnModificar.setEnabled(true);
                vRec.txfDescripcion.setEditable(true);
                vRec.jcEstado.setEnabled(true);
                vRec.txfRendimiento.setEditable(true);
                vRec.txfBarraIngrediente.setEditable(true);
                vRec.txfCantidad.setEditable(true);
                vRec.SetImageBtn(vRec.btnSwitch,"src/imagenes/basura.png");
            } else{
                vRec.btnEliminar1.setEnabled(true);
                vRec.btnModificar.setEnabled(false);
                vRec.txfDescripcion.setEditable(false);
                vRec.jcEstado.setEnabled(false);
                vRec.txfRendimiento.setEditable(false);
                vRec.txfBarraIngrediente.setEditable(false);
                vRec.txfCantidad.setEditable(false);
                vRec.SetImageBtn(vRec.btnSwitch,"src/imagenes/lapiz.png");
            }
        }
        if(vRec.btnEliminar1 == e.getSource()){
            if(mRec.Eliminar(vRec)){
                mRec.consultaR(recetas.tablaRecetas);
                vRec.dispose();
            }
        }
        if(vRec.btnModificar == e.getSource()){
            if(mRec.modReceta(vRec)){
                mRec.consultaR(recetas.tablaRecetas);
                vRec.dispose();
            }
        }
        if(vRec.btnAgregarIngrediente == e.getSource()){
            Object[] fila = new Object[4];
            fila[0] = insumo[0];
            fila[1] = insumo[1];
            fila[2] = Float.parseFloat(vRec.txfCantidad.getText());
            fila[3] = costoInsumo * Float.parseFloat(vRec.txfCantidad.getText());
            vRec.modelo.addRow(fila);
            vRec.txfCostoTotal.setText(""+suma());
            vRec.txfCantidad.setText("");
        }
        if(vRec.btnEliminarIngrediente == e.getSource()){
            vRec.modelo.removeRow(filaEliminar);
            vRec.txfCostoTotal.setText(""+suma());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(vRec.tbBusquedaIngrediente == e.getSource()){
            int  fila = vRec.tbBusquedaIngrediente.rowAtPoint(e.getPoint());
            if (fila > -1){
                idInsumo = String.valueOf(vRec.tbBusquedaIngrediente.getValueAt(fila, 0));
                mRec.getInsumo(this, idInsumo);
                vRec.lblUnidades.setText(insumo[2]);
            }
            if (idInsumo != null && vRec.btnModificar.isEnabled() == true){
                vRec.btnAgregarIngrediente.setEnabled(true);
            }
        } else
        if(vRec.tbIngredientes == e.getSource() && vRec.btnModificar.isEnabled() == true){
            filaEliminar = vRec.tbIngredientes.rowAtPoint(e.getPoint());
            vRec.btnEliminarIngrediente.setEnabled(true);
        }
        else{
            vRec.btnEliminarIngrediente.setEnabled(false);
            vRec.btnAgregarIngrediente.setEnabled(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
       
    }
    
}
