
import Controlador.controladorLogin;
import Modelo.modeloLogin;
import Vista.login;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       login log = new login();
       modeloLogin mlog = new modeloLogin();
       controladorLogin l = new controladorLogin(log,mlog);
       l.LogIn();
    }
}
