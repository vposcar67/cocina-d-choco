package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Conexion {
    
    private String host, port, username, password;
    private Connection con;
    LoadConnectionInfo LCI = new LoadConnectionInfo();

    public Conexion() {
        LCI.CargarinfoConexion();
        this.host = LCI.getHost();
        this.port = LCI.getPort();
        this.username = LCI.getUsername();
        this.password = LCI.getPassword();
    }
    
    public Connection connectToDatabase(){
        try {
            // Construir la URL de conexión
            String url = "jdbc:mysql://" + host + ":" + port + "/cocina" ;

            // Establecer la conexión a la base de datos
            con = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error connecting to database: " + ex.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return con;
    }
    
    public void disconnectDatabase(Connection c){        
        try{
            if(!c.isClosed()){
                c.close();
            }
        }catch(SQLException e){
            System.out.println("Error al cerrar la conexión");
        }        
    }
}

