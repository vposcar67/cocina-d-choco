package Modelo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class LoadConnectionInfo {
    
    private String host, port, username, password;

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    
    public void CargarinfoConexion(){
        Properties properties = new Properties();

        try (FileInputStream inputStream = new FileInputStream("connection.properties")) {
            properties.load(inputStream);

            host = properties.getProperty("host");
            port = properties.getProperty("port");
            username = properties.getProperty("username");
            password = properties.getProperty("password");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
}

