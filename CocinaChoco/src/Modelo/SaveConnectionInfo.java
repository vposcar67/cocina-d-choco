package Modelo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class SaveConnectionInfo {
    
    private String host,port,user,pass;

    public SaveConnectionInfo(String host, String port, String user, String pass) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.pass = pass;
    }
    
    public void GuardarInfoConexion() {
        Properties properties = new Properties();
        properties.setProperty("host", host);
        properties.setProperty("port", port);
        properties.setProperty("username", user);
        properties.setProperty("password", pass);

        try (FileOutputStream outputStream = new FileOutputStream("connection.properties")) {
            properties.store(outputStream, "Database Connection Properties");
            System.out.println("Connection information saved successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
