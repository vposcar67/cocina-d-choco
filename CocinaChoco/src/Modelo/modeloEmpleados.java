
package Modelo;

import Vista.altaEmpleado;
import Vista.eliminarEmpleado;
import Vista.modEmpleado;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class modeloEmpleados {
  public int consultaR(javax.swing.JTable tabla){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL consultaEmpleados();"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean barraBusqueda(javax.swing.JTable tabla, javax.swing.JTextField barra){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String busqueda = barra.getText();
            String cadenaSQL = "SELECT\n" +
                "    idEmpleado as ID,\n" +
                "    nombre as 'Nombre completo',\n" +
                "    teléfono as Teléfono, \n" +
                "    domicilio as Domicilio,\n" +
                "    RFC, contrasena as Contraseña,\n" +
                "    Estado\n" +
                "FROM\n" +
                "    empleados ";
            if(busqueda.charAt(0) >= 48 && busqueda.charAt(0)<= 57 && busqueda.length() <= 3){
               cadenaSQL =  cadenaSQL+"Where idEmpleado LIKE '%"+busqueda+"%' order by Estado;";
            }else {
               cadenaSQL =  cadenaSQL+"Where nombre LIKE '%"+busqueda+"%' or RFC LIKE '%"+busqueda+"%' order by Estado;";
            }

            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }  
    
    public boolean añadir(altaEmpleado aEmp){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL añadirEmpleado('"+aEmp.txfNombre.getText()+"','"+aEmp.txfTelefono.getText()+"','"+aEmp.txfDireccion.getText()+"','"+aEmp.txfRFC.getText()+"','"+aEmp.txfPass.getText()+"')"; 
            ResultSet rs = s.executeQuery(cadenaSQL);
            rs.close();
            JOptionPane.showMessageDialog(aEmp, "Empleado añadido correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch(NumberFormatException e){
            JOptionPane.showMessageDialog(aEmp, "Ingrese los datos correctamente",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
     
    public boolean eliminar(eliminarEmpleado eEmp){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "DELETE FROM empleados WHERE idEmpleado = '"+eEmp.txfID.getText()+"'"; 
            s.execute(cadenaSQL);
            JOptionPane.showMessageDialog(eEmp, "Empleado eliminado correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
    
    public boolean modificar(modEmpleado mEmp){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "UPDATE empleados SET nombre = '"+mEmp.txfNombre.getText()+"', teléfono = '"+mEmp.txfTelefono.getText()+"', domicilio = '"+mEmp.txfDireccion.getText()+"', RFC = '"+mEmp.txfRFC.getText()+"', Estado = '"+mEmp.jcEstado.getSelectedItem()+"', contrasena = '"+mEmp.txfPass.getText()+"' WHERE idEmpleado = '"+mEmp.txfID.getText()+"'"; 
            s.execute(cadenaSQL);
            JOptionPane.showMessageDialog(mEmp, "Empleado modificado correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
}
