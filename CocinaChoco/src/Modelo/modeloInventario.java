/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;
 
import Vista.anadirInventario;
import Vista.eliminarInventario;
import Vista.modInventario;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class modeloInventario {
    
    public int consultaR(javax.swing.JTable tabla){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL consultaR();"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public boolean barraBusqueda(javax.swing.JTable tabla, javax.swing.JTextField barra){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String busqueda = barra.getText();
            String cadenaSQL = null;
            if(busqueda.charAt(0) >= 48 && busqueda.charAt(0)<= 57){
               cadenaSQL =  "SELECT idInsumo as ID, descripcion as Descripción, CONCAT(existencia,' ',unidades) as Existencia, CONCAT('$',costo_unidad) as 'Costo por unidad', Estado from inventario where idInsumo LIKE '%"+busqueda+"%';";
            } else if(busqueda.charAt(0) == 33){
               cadenaSQL =  "SELECT idInsumo as ID, descripcion as Descripción, CONCAT(existencia,' ',unidades) as Existencia, CONCAT('$',costo_unidad) as 'Costo por unidad', Estado from inventario order by Estado, existencia ASC";
            }else if(busqueda.charAt(0) == '$'){
               cadenaSQL =  "SELECT idInsumo as ID, descripcion as Descripción, CONCAT(existencia,' ',unidades) as Existencia, CONCAT('$',costo_unidad) as 'Costo por unidad', Estado from inventario order by Estado, costo_unidad ASC";
            }
            else{
               cadenaSQL =  "SELECT idInsumo as ID, descripcion as Descripción, CONCAT(existencia,' ',unidades) as Existencia, CONCAT('$',costo_unidad) as 'Costo por unidad', Estado from inventario where descripcion LIKE '%"+busqueda+"%' Order by Estado;";
            }
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
        
    }
    
    public boolean añadir(anadirInventario inv){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL añadirInsumo('"+inv.txfDescripcion.getText()+"',"+Float.parseFloat(inv.txfExistencia.getText())+",'"+inv.jcUnidades.getSelectedItem()+"','"+Float.parseFloat(inv.txfCostoUnidad.getText()) +"');"; 
            ResultSet rs = s.executeQuery(cadenaSQL);
            rs.close();
            JOptionPane.showMessageDialog(inv, "Insumo añadido correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch(NumberFormatException e){
            JOptionPane.showMessageDialog(inv, "Ingrese los datos correctamente",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean buscar(eliminarInventario eI){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "Select * from inventario where idInsumo = '"+eI.txfID.getText()+"';";
            ResultSet rs = s.executeQuery(cadenaSQL);
            
            if (rs.next()) {
                eI.txfDescripcion.setText(rs.getString(2));
                eI.txfExistencia.setText(rs.getString(3));
                eI.txfCostoUnidad.setText(rs.getString(5));
                eI.jcUnidades.setSelectedItem(rs.getString(4));
            } 
            
            rs.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(eI, "No se encuentra este insumo o huno algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean buscar(modInventario mI){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "Select * from inventario where idInsumo = '"+mI.txfID.getText()+"';";
            ResultSet rs = s.executeQuery(cadenaSQL);
            
            if (rs.next()) {
                mI.txfDescripcion.setText(rs.getString(2));
                mI.txfExistencia.setText(rs.getString(3));
                mI.txfCostoUnidad.setText(rs.getString(5));
                mI.jcUnidades.setSelectedItem(rs.getString(4));
                mI.jcEstado.setSelectedItem(rs.getString(6));
            } 
            
            rs.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(mI, "No se encuentra este insumo o huno algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean eliminar(eliminarInventario eI){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "DELETE FROM inventario WHERE idInsumo = '"+eI.txfID.getText()+"' AND descripcion = '"+eI.txfDescripcion.getText()+"';";
            s.execute(cadenaSQL);
            JOptionPane.showMessageDialog(eI, "Insumo eliminado correctamente");     
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(eI, "No se encuentra este insumo o hubo algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean modificar(modInventario inv){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "UPDATE inventario SET descripcion = '"+inv.txfDescripcion.getText()+"', existencia = "+Float.parseFloat(inv.txfExistencia.getText())+", unidades = '"+inv.jcUnidades.getSelectedItem()+"', costo_unidad = "+Float.parseFloat(inv.txfCostoUnidad.getText())+", Estado = '"+inv.jcEstado.getSelectedItem()+"' WHERE idInsumo = '"+inv.txfID.getText()+"';"; 
            s.execute(cadenaSQL);
            JOptionPane.showMessageDialog(inv, "Insumo modificado correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch(NumberFormatException e){
            JOptionPane.showMessageDialog(inv, "Ingrese los datos correctamente",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
}
