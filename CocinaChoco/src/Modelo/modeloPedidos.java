/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;


import Controlador.CurrentUser;
import Controlador.controladorAnadirPedido;
import Vista.realizarPedido;
import Vista.verPedido;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class modeloPedidos {
    public int consultaR(javax.swing.JTable tabla){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL consultaPedidos();"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
       public boolean rPedido(realizarPedido rPed, CurrentUser CU){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL = "SELECT idProveedor from proveedores WHERE empresa = '"+rPed.jcProveedor.getSelectedItem().toString()+"'";
            ResultSet rs = s.executeQuery(cadenaSQL);
            String idEmpleado = "";
            if(rs.next()){
                idEmpleado = rs.getString(1);
            }
            
            cadenaSQL =  "CALL añadirPedidoInfo('"+idEmpleado+"','"+CU.getIdEmpleado()+"')";
            s.execute(cadenaSQL);

            for(int i = 0; i < rPed.tbIngredientes.getRowCount(); i++){
                cadenaSQL =  "CALL añadirPedidoInsumos('"+rPed.txfID.getText()+"','"+rPed.tbIngredientes.getValueAt(i,0).toString()+"','"+Float.parseFloat(rPed.tbIngredientes.getValueAt(i,2).toString())+"')";
                s.execute(cadenaSQL);
            }
            JOptionPane.showMessageDialog(rPed, "Pedido realizado exitosamente");        
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(rPed, "Error al añadir o modificar la receta",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean barraBusqueda(javax.swing.JTable tabla, javax.swing.JTextField barra){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String busqueda = barra.getText();
            String cadenaSQL = "SELECT \n" +
                "    p.idPedido as ID,\n" +
                "    pr.empresa as Proveedor,\n" +
                "    p.fecha_pedido as 'Fecha del pedido',\n" +
                "    p.estado as Estado,\n" +
                "    ep.nombre as 'Pedido por',\n" +
                "    p.fecha_recibido as 'Fecha de recibido',\n" +
                "    er.nombre as 'Recibido por'\n" +
                "FROM \n" +
                "    pedidos p\n" +
                "LEFT JOIN \n" +
                "    proveedores pr ON p.Proveedor = pr.idProveedor\n" +
                "LEFT JOIN \n" +
                "    empleados ep ON p.Pedido_por = ep.idEmpleado\n" +
                "LEFT JOIN \n" +
                "    empleados er ON p.Recibido_por = er.idEmpleado ";
            if(busqueda.charAt(0) >= 48 && busqueda.charAt(0)<= 57 && busqueda.length() <= 3){
               cadenaSQL =  cadenaSQL+"Where idPedido LIKE '%"+busqueda+"%' ORDER BY\n" +
"	FIELD(p.estado, 'En proceso', 'Recibido','Cancelado'), p.fecha_pedido DESC;";
            }else if(busqueda.length() > 4 && busqueda.charAt(4) == '-'){
               cadenaSQL =  cadenaSQL+"WHERE fecha_recibido LIKE '%"+busqueda+"%' or fecha_pedido LIKE '%"+busqueda+"%';";
            }
            else {
               cadenaSQL =  cadenaSQL+"Where pr.empresa LIKE '%"+busqueda+"%' or ep.nombre LIKE '%"+busqueda+"%' or er.nombre LIKE '%"+busqueda+"%' ORDER BY\n" +
"	FIELD(p.estado, 'En proceso', 'Recibido','Cancelado'), p.fecha_pedido DESC;";
            }

            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
        
    }
    
    public boolean Cargar(realizarPedido rPed, CurrentUser CU){

        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            
            String cadenaSQL =  "SELECT nombre FROM empleados WHERE idEmpleado = '"+CU.getIdEmpleado()+"';";
            ResultSet rs = s.executeQuery(cadenaSQL);
            if (rs.next()) {
                rPed.txfPedidoPor.setText(rs.getString(1));
            }
            
            cadenaSQL =  "SELECT empresa FROM proveedores WHERE Estado = 'Activo';";
            rs = s.executeQuery(cadenaSQL);
            while (rs.next()) {
                rPed.jcProveedor.addItem(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(rPed, "No se encuentra este proveedor o hubo algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false; 
    }
    
    public boolean buscar(javax.swing.JTextField barra, javax.swing.JTable tabla){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String busqueda = barra.getText();
            String cadenaSQL =  "Select idInsumo as ID,descripcion as Descripción from inventario ";
            if(busqueda.charAt(0) >= 48 && busqueda.charAt(0)<= 57 && busqueda.length() <= 3){
               cadenaSQL =  cadenaSQL+"Where idInsumo LIKE '%"+busqueda+"%' AND Estado = 'Activo';";
            }
            else {
               cadenaSQL =  cadenaSQL+"Where descripcion LIKE '%"+busqueda+"%' AND Estado = 'Activo';";
            }
            
            ResultSet rs = s.executeQuery(cadenaSQL);
            
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
            
            rs.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(tabla, "No se encuentra este insumo o hubo algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean getInsumo(controladorAnadirPedido cAPed,String ID){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "Select idInsumo,descripcion,unidades,costo_unidad from inventario where idInsumo LIKE '%"+ID+"%' AND Estado = 'Activo';";
            ResultSet rs = s.executeQuery(cadenaSQL);

            if (rs.next()){
                cAPed.insumo[0] = rs.getString(1);
                cAPed.insumo[1] = rs.getString(2);
                cAPed.insumo[2] = rs.getString(3);
                cAPed.costoInsumo = Float.parseFloat(rs.getString(4));
            }
            rs.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "No se encuentra este insumo o hubo algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public DefaultTableModel getModelo(String ID){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL modeloListaPedido('"+ID+"');"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }
            rs.close();
            return tableModel;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public boolean cancelar(verPedido vP){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "UPDATE pedidos SET estado = 'Cancelado' WHERE idPedido = '"+vP.txfID.getText()+"'";
            s.execute(cadenaSQL);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(vP, "Error al cancelar el Pedido",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean recibir(verPedido vP, CurrentUser CU){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "UPDATE pedidos SET estado = 'Recibido', fecha_recibido = CURDATE(), Recibido_por = '"+CU.getIdEmpleado()+"' WHERE idPedido = '"+vP.txfID.getText()+"'";
            s.execute(cadenaSQL); 
            cadenaSQL = "CALL ActualizarInventarioPedido('"+vP.txfID.getText()+"');";
            s.execute(cadenaSQL);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(vP, "Error al recibir el Pedido",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

}

