/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Controlador.CurrentUser;
import Vista.anadirPreparacion;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class modeloPreparaciones {
    public int consultaR(javax.swing.JTable tabla){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL consultaPreparaciones();"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public boolean barraBusqueda(javax.swing.JTable tabla, javax.swing.JTextField barra){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String busqueda = barra.getText();
            String cadenaSQL = "SELECT\n" +
                "    pre.idOrden as ID,\n" +
                "    rec.descripcion as Receta,\n" +
                "    pre.cantidad as Cantidad,\n" +
                "    emp.nombre as 'Preparado por',\n" +
                "    pre.fecha_orden as 'Fecha de la preparación'\n" +
                "FROM \n" +
                "    preparacion pre\n" +
                "LEFT JOIN\n" +
                "    recetas rec ON pre.idReceta = rec.idReceta\n" +
                "LEFT JOIN\n" +
                "    empleados emp ON emp.idEmpleado = pre.Preparado_por ";
            if(busqueda.charAt(0) >= 48 && busqueda.charAt(0)<= 57 && busqueda.length() <= 3){
               cadenaSQL =  cadenaSQL+"Where pre.idOrden LIKE '%"+busqueda+"%' order by pre.fecha_orden desc;";
            }else if(busqueda.length() > 4 && busqueda.charAt(4) == '-'){
               cadenaSQL =  cadenaSQL+"WHERE pre.fecha_orden LIKE '%"+busqueda+"%';";
            }
            else {
               cadenaSQL =  cadenaSQL+"Where rec.descripcion LIKE '%"+busqueda+"%' or emp.nombre LIKE '%"+busqueda+"%'order by pre.fecha_orden desc;";
            }

            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
        
    }
    
    public boolean Cargar(anadirPreparacion aPre, CurrentUser CU){

        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            
            String cadenaSQL =  "SELECT nombre FROM empleados WHERE idEmpleado = '"+CU.getIdEmpleado()+"';";
            ResultSet rs = s.executeQuery(cadenaSQL);
            if (rs.next()) {
                aPre.txfPreparadoPor.setText(rs.getString(1));
            }
            
            cadenaSQL =  "SELECT descripcion FROM recetas WHERE Estado = 'Activo';";
            rs = s.executeQuery(cadenaSQL);
            while (rs.next()) {
                aPre.jcRecetas.addItem(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(aPre, "No se encuentra este insumo o hubo algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false; 
    }
    
    public boolean Preparar(anadirPreparacion aPre, CurrentUser CU){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "SELECT idReceta from recetas where descripcion = '"+aPre.jcRecetas.getSelectedItem()+"'"; 
            ResultSet rs = s.executeQuery(cadenaSQL);
            
            String idReceta = null;
            if(rs.next()){
                idReceta = rs.getString(1);
            }
            
            cadenaSQL =  "CALL añadirPreparacion('"+idReceta+"',"+Float.parseFloat(aPre.txfCantidad.getText())+",'"+CU.getIdEmpleado()+"');"; 
            s.execute(cadenaSQL);
            
            rs.close();
            JOptionPane.showMessageDialog(aPre, "Preparación añadida correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch(NumberFormatException e){
            JOptionPane.showMessageDialog(aPre, "Ingrese los datos correctamente",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;

    }
}
