/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;
import Vista.anadirProveedor;
import Vista.eliminarProveedor;
import Vista.modProveedor;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class modeloProveedores {
    public int consultaR(javax.swing.JTable tabla){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL consultaProveedores();"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public boolean barraBusqueda(javax.swing.JTable tabla, javax.swing.JTextField barra){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String busqueda = barra.getText();
            String cadenaSQL = "SELECT\n" +
                "    idProveedor as ID,\n" +
                "    empresa as Proveedor,\n" +
                "    resumen_catalogo as 'Tipos de productos',\n" +
                "    teléfono as Teléfono,\n" +
                "    domicilio as Domicilio,\n" +
                "    horario as Horario,\n" +
                "    Estado\n"+    
                "FROM\n" +
                "    proveedores\n";
            if(busqueda.charAt(0) >= 48 && busqueda.charAt(0)<= 57 && busqueda.length() <= 3){
               cadenaSQL =  cadenaSQL+"Where idProveedor LIKE '%"+busqueda+"%' order by Estado;";
            }
            else {
               cadenaSQL =  cadenaSQL+"Where empresa LIKE '%"+busqueda+"%' or resumen_catalogo LIKE '%"+busqueda+"%' order by Estado;";
            }

            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean añadir(anadirProveedor aProv){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL añadirProveedor('"+aProv.txfEmpresa.getText()+"','"+aProv.txfTelefono.getText()+"','"+aProv.txfDireccion.getText()+"','"+aProv.txfHorario.getText()+"','"+aProv.txfCatálogo.getText()+"')"; 
            ResultSet rs = s.executeQuery(cadenaSQL);
            rs.close();
            JOptionPane.showMessageDialog(aProv, "Proveedor añadido correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch(NumberFormatException e){
            JOptionPane.showMessageDialog(aProv, "Ingrese los datos correctamente",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean eliminar(eliminarProveedor eP){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "DELETE FROM proveedores WHERE idProveedor = '"+eP.txfID.getText()+"'"; 
            s.execute(cadenaSQL);
            JOptionPane.showMessageDialog(eP, "Proveedor eliminado correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
    
    public boolean modificar(modProveedor mP){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "UPDATE proveedores SET empresa = '"+mP.txfEmpresa.getText()+"', teléfono = '"+mP.txfTelefono.getText()+"', domicilio = '"+mP.txfDireccion.getText()+"', horario = '"+mP.txfHorario.getText()+"' ,resumen_catalogo = '"+mP.txfCatálogo.getText()+"', Estado = '"+mP.jcEstado.getSelectedItem()+"' WHERE idProveedor = '"+mP.txfID.getText()+"';"; 
            s.execute(cadenaSQL);
            JOptionPane.showMessageDialog(mP, "Proveedor modificado correctamente");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return false;
    }
}
