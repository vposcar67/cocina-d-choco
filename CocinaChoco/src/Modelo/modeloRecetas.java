/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;
import Controlador.controladorAnadirReceta;
import Controlador.controladorVerReceta;
import Vista.anadirReceta;
import Vista.verReceta;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class modeloRecetas {
    public int consultaR(javax.swing.JTable tabla){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL consultaRecetas();"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public boolean barraBusqueda(javax.swing.JTable tabla, javax.swing.JTextField barra){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String busqueda = barra.getText();
            String cadenaSQL = "SELECT\n" +
                "    idReceta as ID, descripcion as Descripción, CONCAT(rendimiento_preparacion,\" KG\") as Rendimiento, CONCAT('$',costo_preparacion) as 'Costo por preparación', CONCAT(Existencia, ' KG') as 'Cantidad en existencia', Estado\n" +
                "FROM\n" +
                "    recetas ";
            if(busqueda.charAt(0) >= 48 && busqueda.charAt(0)<= 57 && busqueda.length() <= 3){
               cadenaSQL =  cadenaSQL+"Where idReceta LIKE '%"+busqueda+"%' order by Estado;";
            }else if(busqueda.charAt(0) == '!'){
               cadenaSQL =  cadenaSQL+"ORDER BY Estado, Existencia ASC;";
            }else if(busqueda.charAt(0) == '$'){
               cadenaSQL =  cadenaSQL+"ORDER BY Estado, costo_preparacion ASC;";
            }
            else {
               cadenaSQL =  cadenaSQL+"Where descripcion LIKE '%"+busqueda+"%' ORDER BY Estado ;";
            }

            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
        
    }
    
    public boolean buscar(javax.swing.JTextField barra, javax.swing.JTable tabla){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String busqueda = barra.getText();
            String cadenaSQL =  "Select idInsumo as ID,descripcion as Descripción from inventario ";
            if(busqueda.charAt(0) >= 48 && busqueda.charAt(0)<= 57 && busqueda.length() <= 3){
               cadenaSQL =  cadenaSQL+"Where idInsumo LIKE '%"+busqueda+"%' AND Estado = 'Activo';";
            }
            else {
               cadenaSQL =  cadenaSQL+"Where descripcion LIKE '%"+busqueda+"%' AND Estado = 'Activo';";
            }
            
            ResultSet rs = s.executeQuery(cadenaSQL);
            
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }

            tabla.setModel(tableModel);

            rs.close();
            
            rs.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(tabla, "No se encuentra este insumo o hubo algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean getInsumo(controladorAnadirReceta cARec,String ID){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "Select idInsumo,descripcion,unidades,costo_unidad from inventario where idInsumo LIKE '%"+ID+"%' AND Estado = 'Activo';";
            ResultSet rs = s.executeQuery(cadenaSQL);

            if (rs.next()){
                cARec.insumo[0] = rs.getString(1);
                cARec.insumo[1] = rs.getString(2);
                cARec.insumo[2] = rs.getString(3);
                cARec.costoInsumo = Float.parseFloat(rs.getString(4));
            }
            rs.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "No se encuentra este insumo o hubo algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean getInsumo(controladorVerReceta cARec,String ID){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "Select idInsumo,descripcion,unidades,costo_unidad from inventario where idInsumo LIKE '%"+ID+"%' AND Estado = 'Activo';";
            ResultSet rs = s.executeQuery(cadenaSQL);

            if (rs.next()){
                cARec.insumo[0] = rs.getString(1);
                cARec.insumo[1] = rs.getString(2);
                cARec.insumo[2] = rs.getString(3);
                cARec.costoInsumo = Float.parseFloat(rs.getString(4));
            }
            rs.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "No se encuentra este insumo o hubo algún error",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public DefaultTableModel getModelo(String ID){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL modeloIngredientesReceta('"+ID+"');"; 
            ResultSet rs = s.executeQuery(cadenaSQL);

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            DefaultTableModel tableModel = new DefaultTableModel();

            for (int i = 1; i <= columnCount; i++) {
                tableModel.addColumn(metaData.getColumnLabel(i));
            }

            while (rs.next()) {
                Object[] rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    rowData[i - 1] = rs.getObject(i);
                }
                tableModel.addRow(rowData);
            }
            rs.close();
            return tableModel;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public boolean añadirReceta(anadirReceta aR){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "CALL añadirRecetaInfo('"+aR.txfDescripcion.getText()+"','"+Float.parseFloat(aR.txfRendimiento.getText())+"','"+Float.parseFloat(aR.txfCostoTotal.getText())+"')";
            s.execute(cadenaSQL);

            for(int i = 0; i < aR.tbIngredientes.getRowCount(); i++){
                cadenaSQL =  "CALL añadirRecetaIngredientes('"+aR.txfID.getText()+"','"+aR.tbIngredientes.getValueAt(i,0).toString()+"','"+Float.parseFloat(aR.tbIngredientes.getValueAt(i,2).toString())+"')";
                s.execute(cadenaSQL);
                System.out.println("Prueba 3."+i);
            }
            JOptionPane.showMessageDialog(aR, "Receta añadida exitosamente");        
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(aR, "Error al añadir o modificar la receta",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean Eliminar(verReceta vR){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "DELETE FROM recetas_insumos WHERE idReceta = '"+vR.txfID.getText()+"'";
            s.execute(cadenaSQL);
            cadenaSQL =  "DELETE FROM recetas WHERE idReceta = '"+vR.txfID.getText()+"'";
            s.execute(cadenaSQL);
            
            JOptionPane.showMessageDialog(vR, "Receta Eliminada exitosamente");        
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(vR, "Error al eliminar la receta",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
    public boolean modReceta(verReceta aR){
        try {
            Conexion conexion = new Conexion();
            Connection con = conexion.connectToDatabase();            
            Statement s = con.createStatement();
            String cadenaSQL =  "UPDATE recetas SET descripcion = '"+aR.txfDescripcion.getText()+"', rendimiento_preparacion = "+Float.parseFloat(aR.txfRendimiento.getText())+", costo_preparacion = "+Float.parseFloat(aR.txfCostoTotal.getText())+", Estado = '"+aR.jcEstado.getSelectedItem().toString()+"' WHERE idReceta = '"+aR.txfID.getText()+"';";
            s.execute(cadenaSQL);

            cadenaSQL = "DELETE FROM recetas_insumos WHERE idReceta = '"+aR.txfID.getText()+"'";
            s.execute(cadenaSQL);
            for(int i = 0; i < aR.tbIngredientes.getRowCount(); i++){
                cadenaSQL =  "CALL añadirRecetaIngredientes('"+aR.txfID.getText()+"','"+aR.tbIngredientes.getValueAt(i,0).toString()+"','"+Float.parseFloat(aR.tbIngredientes.getValueAt(i,2).toString())+"')";
                s.execute(cadenaSQL);
            }
            
            JOptionPane.showMessageDialog(aR, "Receta modificada exitosamente");        
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(aR, "Error al añadir o modificar la receta",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }
    
}
