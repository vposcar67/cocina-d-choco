/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Repositorio;

import Controlador.CurrentUser;
import Controlador.SalirInicio;
import Repositorio.modeloVentas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author oscar
 */
public class controladorVentas implements ActionListener, KeyListener{

    private CurrentUser CU;
    private modeloVentas mVen;
    private ventas ventas;

    public controladorVentas(CurrentUser CU, modeloVentas mVen, ventas ventas) {
        this.CU = CU;
        this.mVen = mVen;
        this.ventas = ventas;
        this.ventas.btnActualizar.addActionListener(this);
        this.ventas.btnSalir.addActionListener(this);
        this.ventas.btnVender.addActionListener(this);
        mVen.consultaR(ventas.tablaVentas);
        this.ventas.txtID.addKeyListener(this);
    }
          
    @Override
    public void actionPerformed(ActionEvent e) {
        if(ventas.btnSalir == e.getSource()){
            SalirInicio SI = new SalirInicio(CU,ventas);
            SI.Salir();
        }
        if(ventas.btnActualizar == e.getSource()){
            mVen.consultaR(ventas.tablaVentas);
            ventas.txtID.setText("");
        }
        if(ventas.btnVender == e.getSource()){
            System.out.println("Vender");
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        mVen.barraBusqueda(ventas.tablaVentas, ventas.txtID);
    }
    
}
