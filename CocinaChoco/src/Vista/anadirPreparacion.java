/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

/**
 *
 * @author oscar
 */
public class anadirPreparacion extends javax.swing.JFrame {
    
    public anadirPreparacion(String ID) {
        initComponents();
        this.txfID.setText(ID);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg = new javax.swing.JPanel();
        lblTitulo1 = new javax.swing.JLabel();
        lblcocina = new javax.swing.JLabel();
        btnPreparar = new javax.swing.JButton();
        lblID = new javax.swing.JLabel();
        txfID = new javax.swing.JTextField();
        lblID1 = new javax.swing.JLabel();
        txfPreparadoPor = new javax.swing.JTextField();
        lblID3 = new javax.swing.JLabel();
        lblID6 = new javax.swing.JLabel();
        jcRecetas = new javax.swing.JComboBox<>();
        txfCantidad = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Preparar una Receta");

        bg.setBackground(new java.awt.Color(255, 255, 255));
        bg.setMinimumSize(new java.awt.Dimension(850, 570));
        bg.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitulo1.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        lblTitulo1.setForeground(new java.awt.Color(255, 0, 0));
        lblTitulo1.setText("PREPARAR RECETA");
        bg.add(lblTitulo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 410, 59));

        lblcocina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/logo cocina.jpg"))); // NOI18N
        bg.add(lblcocina, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, 250, 65));

        btnPreparar.setBackground(new java.awt.Color(255, 255, 102));
        btnPreparar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnPreparar.setText("PREPARAR");
        btnPreparar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bg.add(btnPreparar, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 300, 160, 60));

        lblID.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID.setForeground(new java.awt.Color(102, 102, 102));
        lblID.setText("ID:");
        bg.add(lblID, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 110, 30, 20));

        txfID.setEditable(false);
        txfID.setForeground(new java.awt.Color(51, 51, 51));
        txfID.setToolTipText("");
        txfID.setBorder(null);
        bg.add(txfID, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 110, 70, 20));

        lblID1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID1.setForeground(new java.awt.Color(102, 102, 102));
        lblID1.setText("Cantidad:");
        bg.add(lblID1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, 160, -1));

        txfPreparadoPor.setEditable(false);
        txfPreparadoPor.setForeground(new java.awt.Color(51, 51, 51));
        txfPreparadoPor.setToolTipText("");
        txfPreparadoPor.setBorder(null);
        bg.add(txfPreparadoPor, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 110, 250, 20));

        lblID3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID3.setForeground(new java.awt.Color(102, 102, 102));
        lblID3.setText("Preparado por: ");
        bg.add(lblID3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 110, 140, -1));

        lblID6.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID6.setForeground(new java.awt.Color(102, 102, 102));
        lblID6.setText("Receta:");
        bg.add(lblID6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 150, 90, -1));

        jcRecetas.setForeground(new java.awt.Color(51, 51, 51));
        bg.add(jcRecetas, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 180, 520, -1));

        txfCantidad.setForeground(new java.awt.Color(51, 51, 51));
        txfCantidad.setToolTipText("");
        txfCantidad.setBorder(null);
        bg.add(txfCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 250, 150, 20));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 270, 150, 10));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 626, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bg;
    public javax.swing.JButton btnPreparar;
    private javax.swing.JSeparator jSeparator3;
    public javax.swing.JComboBox<String> jcRecetas;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblID1;
    private javax.swing.JLabel lblID3;
    private javax.swing.JLabel lblID6;
    private javax.swing.JLabel lblTitulo1;
    private javax.swing.JLabel lblcocina;
    public javax.swing.JTextField txfCantidad;
    public javax.swing.JTextField txfID;
    public javax.swing.JTextField txfPreparadoPor;
    // End of variables declaration//GEN-END:variables
}
