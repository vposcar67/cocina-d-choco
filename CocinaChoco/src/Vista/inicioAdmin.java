/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

import Controlador.CurrentUser;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author PC
 */
public class inicioAdmin extends javax.swing.JFrame {
    
    public inicioAdmin(String nombre) {
        initComponents();
        lblTitulo.setText("Bienvenido "+nombre+"!!");
    }

    public void SetImageLabel (JLabel lblName, String root){
        ImageIcon image = new ImageIcon(root);
        Icon icon = new ImageIcon(image.getImage().getScaledInstance(lblName.getWidth(), lblName.getHeight(), Image.SCALE_DEFAULT));
        lblName.setIcon(icon);
        this.repaint();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblcocina = new javax.swing.JLabel();
        lblRango = new javax.swing.JLabel();
        btnAdministrar = new javax.swing.JButton();
        btnPreparaciones = new javax.swing.JButton();
        btnPedidos = new javax.swing.JButton();
        btnInventario1 = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        lblID1 = new javax.swing.JLabel();
        lblPenguin = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Menu Principal");

        bg.setBackground(new java.awt.Color(255, 255, 255));
        bg.setMinimumSize(new java.awt.Dimension(850, 570));
        bg.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitulo.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo.setText("BIENVENDIDO LUIS!!!");
        bg.add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 540, 59));

        lblcocina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/logo cocina.jpg"))); // NOI18N
        bg.add(lblcocina, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 20, 260, 65));

        lblRango.setFont(new java.awt.Font("Segoe UI Emoji", 1, 18)); // NOI18N
        lblRango.setForeground(new java.awt.Color(102, 102, 102));
        lblRango.setText("ADMINISTRADOR");
        bg.add(lblRango, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 200, -1));

        btnAdministrar.setBackground(new java.awt.Color(255, 255, 102));
        btnAdministrar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnAdministrar.setText("ADMINISTRAR");
        btnAdministrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdministrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdministrarActionPerformed(evt);
            }
        });
        bg.add(btnAdministrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 300, 250, 80));

        btnPreparaciones.setBackground(new java.awt.Color(255, 255, 102));
        btnPreparaciones.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnPreparaciones.setText("PREPARACIONES");
        btnPreparaciones.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPreparaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreparacionesActionPerformed(evt);
            }
        });
        bg.add(btnPreparaciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 300, 250, 80));

        btnPedidos.setBackground(new java.awt.Color(255, 255, 102));
        btnPedidos.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnPedidos.setText("PEDIDOS");
        btnPedidos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPedidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPedidosActionPerformed(evt);
            }
        });
        bg.add(btnPedidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 190, 250, 80));

        btnInventario1.setBackground(new java.awt.Color(255, 255, 102));
        btnInventario1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnInventario1.setText("INVENTARIO");
        btnInventario1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnInventario1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInventario1ActionPerformed(evt);
            }
        });
        bg.add(btnInventario1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, 250, 80));

        btnSalir.setBackground(new java.awt.Color(255, 153, 51));
        btnSalir.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnSalir.setText("SALIR");
        btnSalir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        bg.add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 460, 150, 60));

        lblID1.setFont(new java.awt.Font("Segoe UI Emoji", 1, 18)); // NOI18N
        lblID1.setForeground(new java.awt.Color(102, 102, 102));
        lblID1.setText("Que deseas hacer?");
        bg.add(lblID1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 200, -1));
        bg.add(lblPenguin, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 410, 240, 140));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnInventario1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInventario1ActionPerformed

    }//GEN-LAST:event_btnInventario1ActionPerformed

    private void btnPedidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPedidosActionPerformed

    }//GEN-LAST:event_btnPedidosActionPerformed

    private void btnPreparacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreparacionesActionPerformed

    }//GEN-LAST:event_btnPreparacionesActionPerformed

    private void btnAdministrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdministrarActionPerformed

    }//GEN-LAST:event_btnAdministrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bg;
    public javax.swing.JButton btnAdministrar;
    public javax.swing.JButton btnInventario1;
    public javax.swing.JButton btnPedidos;
    public javax.swing.JButton btnPreparaciones;
    public javax.swing.JButton btnSalir;
    private javax.swing.JLabel lblID1;
    public javax.swing.JLabel lblPenguin;
    public javax.swing.JLabel lblRango;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblcocina;
    // End of variables declaration//GEN-END:variables
}
