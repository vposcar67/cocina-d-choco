/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

import java.awt.Color;

/**
 *
 * @author PC
 */
public class modEmpleado extends javax.swing.JFrame {
    
    public modEmpleado(String ID, String nombre, String telefono, String domicilio, String RFC, String contraseña, String Estado) {
        initComponents();
        this.txfID.setText(ID);
        this.txfNombre.setText(nombre);
        this.txfTelefono.setText(telefono);
        this.txfDireccion.setText(domicilio);
        this.txfRFC.setText(RFC);
        this.txfPass.setText(contraseña);
        this.jcEstado.setSelectedItem(Estado);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblcocina = new javax.swing.JLabel();
        btnModificar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        lblID = new javax.swing.JLabel();
        txfNombre = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        lblID1 = new javax.swing.JLabel();
        txfTelefono = new javax.swing.JTextField();
        txfID = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        lblID2 = new javax.swing.JLabel();
        txfRFC = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        lblID3 = new javax.swing.JLabel();
        txfDireccion = new javax.swing.JTextField();
        lblID4 = new javax.swing.JLabel();
        lblID6 = new javax.swing.JLabel();
        txfPass = new javax.swing.JPasswordField();
        jSeparator7 = new javax.swing.JSeparator();
        lblID5 = new javax.swing.JLabel();
        jcEstado = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Modificar un Empleado");

        bg.setBackground(new java.awt.Color(255, 255, 255));
        bg.setMinimumSize(new java.awt.Dimension(850, 570));
        bg.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitulo.setFont(new java.awt.Font("Segoe UI", 1, 30)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(255, 0, 0));
        lblTitulo.setText("MODIFICAR EMPLEADO");
        bg.add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 380, 59));

        lblcocina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/logo cocina.jpg"))); // NOI18N
        bg.add(lblcocina, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 10, 260, 65));

        btnModificar.setBackground(new java.awt.Color(255, 255, 102));
        btnModificar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnModificar.setText("MODIFICAR");
        btnModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        bg.add(btnModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 340, 160, 60));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 300, 10));

        lblID.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID.setForeground(new java.awt.Color(102, 102, 102));
        lblID.setText("Nombre del Empleado:");
        bg.add(lblID, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 200, -1));

        txfNombre.setForeground(new java.awt.Color(51, 51, 51));
        txfNombre.setToolTipText("");
        txfNombre.setBorder(null);
        txfNombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfNombreMousePressed(evt);
            }
        });
        txfNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfNombreActionPerformed(evt);
            }
        });
        bg.add(txfNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, 300, 20));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 310, 300, 10));

        lblID1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID1.setForeground(new java.awt.Color(102, 102, 102));
        lblID1.setText("Teléfono:");
        bg.add(lblID1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, 200, -1));

        txfTelefono.setForeground(new java.awt.Color(51, 51, 51));
        txfTelefono.setToolTipText("");
        txfTelefono.setBorder(null);
        txfTelefono.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfTelefonoMousePressed(evt);
            }
        });
        txfTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfTelefonoActionPerformed(evt);
            }
        });
        bg.add(txfTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 300, 20));

        txfID.setEditable(false);
        txfID.setForeground(new java.awt.Color(51, 51, 51));
        txfID.setToolTipText("");
        txfID.setBorder(null);
        txfID.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfIDMousePressed(evt);
            }
        });
        txfID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfIDActionPerformed(evt);
            }
        });
        bg.add(txfID, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 80, 70, 20));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 300, 10));

        lblID2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID2.setForeground(new java.awt.Color(102, 102, 102));
        lblID2.setText("ID:");
        bg.add(lblID2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 30, 20));

        txfRFC.setForeground(new java.awt.Color(51, 51, 51));
        txfRFC.setToolTipText("");
        txfRFC.setBorder(null);
        txfRFC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfRFCMousePressed(evt);
            }
        });
        txfRFC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfRFCActionPerformed(evt);
            }
        });
        bg.add(txfRFC, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 220, 300, 20));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 240, 300, 10));

        lblID3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID3.setForeground(new java.awt.Color(102, 102, 102));
        lblID3.setText("RFC:");
        bg.add(lblID3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 180, 200, -1));

        txfDireccion.setForeground(new java.awt.Color(51, 51, 51));
        txfDireccion.setToolTipText("");
        txfDireccion.setBorder(null);
        txfDireccion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfDireccionMousePressed(evt);
            }
        });
        txfDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfDireccionActionPerformed(evt);
            }
        });
        bg.add(txfDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, 300, 20));

        lblID4.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID4.setForeground(new java.awt.Color(102, 102, 102));
        lblID4.setText("Estado: ");
        bg.add(lblID4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 260, 200, -1));

        lblID6.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID6.setForeground(new java.awt.Color(102, 102, 102));
        lblID6.setText("Contraseña:");
        bg.add(lblID6, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 110, 130, -1));

        txfPass.setForeground(new java.awt.Color(51, 51, 51));
        txfPass.setToolTipText("");
        txfPass.setBorder(null);
        txfPass.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txfPass.setDoubleBuffered(true);
        txfPass.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfPassMousePressed(evt);
            }
        });
        bg.add(txfPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 150, 300, 20));

        jSeparator7.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 170, 300, 10));

        lblID5.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID5.setForeground(new java.awt.Color(102, 102, 102));
        lblID5.setText("Dirección:");
        bg.add(lblID5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 260, 200, -1));

        jcEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Activo", "Inactivo" }));
        bg.add(jcEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 290, 300, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.PREFERRED_SIZE, 701, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnModificarActionPerformed

    private void txfNombreMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfNombreMousePressed
        if (txfNombre.getText().equals("Ingresar ID del Usuario")){
            txfNombre.setText("");
            txfNombre.setForeground(Color.black);
        }

    }//GEN-LAST:event_txfNombreMousePressed

    private void txfNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfNombreActionPerformed

    private void txfTelefonoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfTelefonoMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfTelefonoMousePressed

    private void txfTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfTelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfTelefonoActionPerformed

    private void txfRFCMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfRFCMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfRFCMousePressed

    private void txfRFCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfRFCActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfRFCActionPerformed

    private void txfDireccionMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfDireccionMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfDireccionMousePressed

    private void txfDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfDireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfDireccionActionPerformed

    private void txfPassMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfPassMousePressed
        if (txfPass.getText().equals("********")){
            txfPass.setText("");
            txfPass.setForeground(Color.black);
        }
    }//GEN-LAST:event_txfPassMousePressed

    private void txfIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfIDActionPerformed

    private void txfIDMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfIDMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfIDMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bg;
    public javax.swing.JButton btnModificar;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator7;
    public javax.swing.JComboBox<String> jcEstado;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblID1;
    private javax.swing.JLabel lblID2;
    private javax.swing.JLabel lblID3;
    private javax.swing.JLabel lblID4;
    private javax.swing.JLabel lblID5;
    private javax.swing.JLabel lblID6;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblcocina;
    public javax.swing.JTextField txfDireccion;
    public javax.swing.JTextField txfID;
    public javax.swing.JTextField txfNombre;
    public javax.swing.JPasswordField txfPass;
    public javax.swing.JTextField txfRFC;
    public javax.swing.JTextField txfTelefono;
    // End of variables declaration//GEN-END:variables
}
