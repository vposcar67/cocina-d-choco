/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

/**
 *
 * @author oscar
 */
public class modInventario extends javax.swing.JFrame {
    
   public modInventario(String ID, String desc, String cantidad, String unidades, String costo, String estado){
        initComponents();
        this.txfID.setText(ID);
        this.txfDescripcion.setText(desc);
        this.txfExistencia.setText(cantidad);
        this.jcUnidades.setSelectedItem(unidades);
        this.txfCostoUnidad.setText(costo);
        this.jcEstado.setSelectedItem(estado);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg = new javax.swing.JPanel();
        lblTitulo1 = new javax.swing.JLabel();
        lblcocina = new javax.swing.JLabel();
        btnModificar = new javax.swing.JButton();
        lblID = new javax.swing.JLabel();
        txfID = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        lblID1 = new javax.swing.JLabel();
        txfCostoUnidad = new javax.swing.JTextField();
        lblID2 = new javax.swing.JLabel();
        txfDescripcion = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        lblID3 = new javax.swing.JLabel();
        lblID6 = new javax.swing.JLabel();
        jcEstado = new javax.swing.JComboBox<>();
        jcUnidades = new javax.swing.JComboBox<>();
        lblID4 = new javax.swing.JLabel();
        txfExistencia = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Modificar un Insumo del Inventario");

        bg.setBackground(new java.awt.Color(255, 255, 255));
        bg.setMinimumSize(new java.awt.Dimension(850, 570));
        bg.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitulo1.setFont(new java.awt.Font("Segoe UI", 1, 30)); // NOI18N
        lblTitulo1.setForeground(new java.awt.Color(255, 0, 0));
        lblTitulo1.setText("MODIFICAR INSUMO");
        bg.add(lblTitulo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 410, 59));

        lblcocina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/logo cocina.jpg"))); // NOI18N
        bg.add(lblcocina, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 20, 260, 65));

        btnModificar.setBackground(new java.awt.Color(255, 255, 102));
        btnModificar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bg.add(btnModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 340, 160, 60));

        lblID.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID.setForeground(new java.awt.Color(102, 102, 102));
        lblID.setText("ID:");
        bg.add(lblID, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 30, 20));

        txfID.setEditable(false);
        txfID.setForeground(new java.awt.Color(51, 51, 51));
        txfID.setToolTipText("");
        txfID.setBorder(null);
        bg.add(txfID, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 90, 70, 20));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 240, 150, 10));

        lblID1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID1.setForeground(new java.awt.Color(102, 102, 102));
        lblID1.setText("Existencia:");
        bg.add(lblID1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 190, 160, -1));

        txfCostoUnidad.setForeground(new java.awt.Color(51, 51, 51));
        txfCostoUnidad.setToolTipText("");
        txfCostoUnidad.setBorder(null);
        bg.add(txfCostoUnidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 220, 150, 20));

        lblID2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID2.setForeground(new java.awt.Color(102, 102, 102));
        lblID2.setText("Estado:");
        bg.add(lblID2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 260, 200, 30));

        txfDescripcion.setForeground(new java.awt.Color(51, 51, 51));
        txfDescripcion.setToolTipText("");
        txfDescripcion.setBorder(null);
        bg.add(txfDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, 530, 20));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 530, 10));

        lblID3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID3.setForeground(new java.awt.Color(102, 102, 102));
        lblID3.setText("Descripción:");
        bg.add(lblID3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, 200, -1));

        lblID6.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID6.setForeground(new java.awt.Color(102, 102, 102));
        lblID6.setText("Unidades:");
        bg.add(lblID6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, 90, -1));

        jcEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Activo", "Inactivo" }));
        bg.add(jcEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, 300, 20));

        jcUnidades.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "KG", "L", "PZA" }));
        bg.add(jcUnidades, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, -1, -1));

        lblID4.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID4.setForeground(new java.awt.Color(102, 102, 102));
        lblID4.setText("Costo por unidad:");
        bg.add(lblID4, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 190, 160, -1));

        txfExistencia.setForeground(new java.awt.Color(51, 51, 51));
        txfExistencia.setToolTipText("");
        txfExistencia.setBorder(null);
        bg.add(txfExistencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 220, 150, 20));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 240, 150, 10));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(bg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.PREFERRED_SIZE, 424, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bg;
    public javax.swing.JButton btnModificar;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    public javax.swing.JComboBox<String> jcEstado;
    public javax.swing.JComboBox<String> jcUnidades;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblID1;
    private javax.swing.JLabel lblID2;
    private javax.swing.JLabel lblID3;
    private javax.swing.JLabel lblID4;
    private javax.swing.JLabel lblID6;
    private javax.swing.JLabel lblTitulo1;
    private javax.swing.JLabel lblcocina;
    public javax.swing.JTextField txfCostoUnidad;
    public javax.swing.JTextField txfDescripcion;
    public javax.swing.JTextField txfExistencia;
    public javax.swing.JTextField txfID;
    // End of variables declaration//GEN-END:variables
}
