/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

import java.awt.Color;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PC
 */
public class verReceta extends javax.swing.JFrame {
    
    public DefaultTableModel modelo;
    public verReceta(String ID, String descripcion, String rendimiento, String costo, String estado, DefaultTableModel modelo){
        initComponents();
        this.txfID.setText(ID);
        this.txfDescripcion.setText(descripcion);
        this.txfRendimiento.setText(rendimiento);
        this.txfCostoTotal.setText(costo.substring(1));
        this.jcEstado.setSelectedItem(estado);
        this.modelo = modelo;
        this.tbIngredientes.setModel(modelo);
        SetImageBtn(this.btnSwitch,"src/imagenes/basura.png");
    }

    public void SetImageBtn (JButton lblName, String root){
        ImageIcon image = new ImageIcon(root);
        Icon icon = new ImageIcon(image.getImage().getScaledInstance(25, 25, Image.SCALE_DEFAULT));
        lblName.setIcon(icon);
        this.repaint();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblcocina = new javax.swing.JLabel();
        btnModificar = new javax.swing.JButton();
        lblID = new javax.swing.JLabel();
        txfDescripcion = new javax.swing.JTextField();
        txfID = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        lblID4 = new javax.swing.JLabel();
        lblID6 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        jcEstado = new javax.swing.JComboBox<>();
        txfRendimiento = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        lblID7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbBusquedaIngrediente = new javax.swing.JTable();
        txfBarraIngrediente = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbIngredientes = new javax.swing.JTable();
        lblTitulo1 = new javax.swing.JLabel();
        txfCantidad = new javax.swing.JTextField();
        lblUnidades = new javax.swing.JLabel();
        btnEliminarIngrediente = new javax.swing.JButton();
        txfCostoTotal = new javax.swing.JTextField();
        lblTitulo2 = new javax.swing.JLabel();
        btnAgregarIngrediente = new javax.swing.JButton();
        btnEliminar1 = new javax.swing.JButton();
        btnSwitch = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ver una Receta");

        bg.setBackground(new java.awt.Color(255, 255, 255));
        bg.setMinimumSize(new java.awt.Dimension(850, 570));
        bg.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitulo.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo.setText("Costo Total:");
        bg.add(lblTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 170, 310, 30));

        lblcocina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/logo cocina.jpg"))); // NOI18N
        bg.add(lblcocina, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 20, 260, 65));

        btnModificar.setBackground(new java.awt.Color(255, 255, 102));
        btnModificar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnModificar.setText("MODIFICAR");
        btnModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        bg.add(btnModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 450, 160, 60));

        lblID.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID.setForeground(new java.awt.Color(102, 102, 102));
        lblID.setText("Descripción:");
        bg.add(lblID, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 200, -1));

        txfDescripcion.setForeground(new java.awt.Color(51, 51, 51));
        txfDescripcion.setToolTipText("");
        txfDescripcion.setBorder(null);
        txfDescripcion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfDescripcionMousePressed(evt);
            }
        });
        txfDescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfDescripcionActionPerformed(evt);
            }
        });
        bg.add(txfDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 470, 20));

        txfID.setEditable(false);
        txfID.setForeground(new java.awt.Color(51, 51, 51));
        txfID.setToolTipText("");
        txfID.setBorder(null);
        txfID.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfIDMousePressed(evt);
            }
        });
        txfID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfIDActionPerformed(evt);
            }
        });
        bg.add(txfID, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 80, 70, 20));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, 470, 10));

        lblID4.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID4.setForeground(new java.awt.Color(102, 102, 102));
        lblID4.setText("Estado: ");
        bg.add(lblID4, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 80, 70, -1));

        lblID6.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID6.setForeground(new java.awt.Color(102, 102, 102));
        lblID6.setText("Rendimiento:");
        bg.add(lblID6, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 110, 120, -1));

        jSeparator7.setForeground(new java.awt.Color(0, 0, 0));
        bg.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 160, 120, 10));

        jcEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Activo", "Inactivo" }));
        bg.add(jcEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 80, 80, -1));

        txfRendimiento.setForeground(new java.awt.Color(51, 51, 51));
        txfRendimiento.setToolTipText("");
        txfRendimiento.setBorder(null);
        txfRendimiento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txfRendimientoMousePressed(evt);
            }
        });
        txfRendimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfRendimientoActionPerformed(evt);
            }
        });
        bg.add(txfRendimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 140, 120, 20));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText(" KG");
        bg.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 140, -1, -1));

        lblID7.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblID7.setForeground(new java.awt.Color(102, 102, 102));
        lblID7.setText("ID:");
        bg.add(lblID7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 30, 20));

        tbBusquedaIngrediente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Descripción"
            }
        ));
        jScrollPane1.setViewportView(tbBusquedaIngrediente);

        bg.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 160, 190));

        txfBarraIngrediente.setForeground(new java.awt.Color(51, 51, 51));
        txfBarraIngrediente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txfBarraIngredienteKeyReleased(evt);
            }
        });
        bg.add(txfBarraIngrediente, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, 160, -1));

        tbIngredientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Ingrediente", "Cantidad", "Costo"
            }
        ));
        jScrollPane2.setViewportView(tbIngredientes);

        bg.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 240, 460, 190));

        lblTitulo1.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        lblTitulo1.setForeground(new java.awt.Color(255, 0, 0));
        lblTitulo1.setText("VER RECETA");
        bg.add(lblTitulo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 310, 59));

        txfCantidad.setForeground(new java.awt.Color(51, 51, 51));
        bg.add(txfCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 210, 70, -1));

        lblUnidades.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        lblUnidades.setForeground(new java.awt.Color(0, 0, 0));
        lblUnidades.setText(" KG");
        bg.add(lblUnidades, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 210, -1, -1));

        btnEliminarIngrediente.setText("Eliminar");
        btnEliminarIngrediente.setEnabled(false);
        btnEliminarIngrediente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarIngredienteActionPerformed(evt);
            }
        });
        bg.add(btnEliminarIngrediente, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 210, 80, -1));

        txfCostoTotal.setEditable(false);
        txfCostoTotal.setForeground(new java.awt.Color(51, 51, 51));
        txfCostoTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txfCostoTotalActionPerformed(evt);
            }
        });
        bg.add(txfCostoTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 210, 150, -1));

        lblTitulo2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lblTitulo2.setForeground(new java.awt.Color(102, 102, 102));
        lblTitulo2.setText("Ingredientes:");
        bg.add(lblTitulo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 310, 30));

        btnAgregarIngrediente.setText("Agregar");
        btnAgregarIngrediente.setEnabled(false);
        btnAgregarIngrediente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarIngredienteActionPerformed(evt);
            }
        });
        bg.add(btnAgregarIngrediente, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 210, 80, -1));

        btnEliminar1.setBackground(new java.awt.Color(255, 255, 102));
        btnEliminar1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        btnEliminar1.setText("ELIMINAR");
        btnEliminar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEliminar1.setEnabled(false);
        btnEliminar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminar1ActionPerformed(evt);
            }
        });
        bg.add(btnEliminar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 450, 160, 60));
        bg.add(btnSwitch, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 80, 40, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.PREFERRED_SIZE, 701, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bg, javax.swing.GroupLayout.PREFERRED_SIZE, 537, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnModificarActionPerformed

    private void txfDescripcionMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfDescripcionMousePressed
        if (txfDescripcion.getText().equals("Ingresar ID del Usuario")){
            txfDescripcion.setText("");
            txfDescripcion.setForeground(Color.black);
        }

    }//GEN-LAST:event_txfDescripcionMousePressed

    private void txfDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfDescripcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfDescripcionActionPerformed

    private void txfIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfIDActionPerformed

    private void txfIDMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfIDMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfIDMousePressed

    private void txfRendimientoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txfRendimientoMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfRendimientoMousePressed

    private void txfRendimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfRendimientoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfRendimientoActionPerformed

    private void txfCostoTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txfCostoTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txfCostoTotalActionPerformed

    private void btnEliminarIngredienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarIngredienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEliminarIngredienteActionPerformed

    private void btnAgregarIngredienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarIngredienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAgregarIngredienteActionPerformed

    private void txfBarraIngredienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txfBarraIngredienteKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txfBarraIngredienteKeyReleased

    private void btnEliminar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminar1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEliminar1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bg;
    public javax.swing.JButton btnAgregarIngrediente;
    public javax.swing.JButton btnEliminar1;
    public javax.swing.JButton btnEliminarIngrediente;
    public javax.swing.JButton btnModificar;
    public javax.swing.JButton btnSwitch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator7;
    public javax.swing.JComboBox<String> jcEstado;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblID4;
    private javax.swing.JLabel lblID6;
    private javax.swing.JLabel lblID7;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblTitulo1;
    private javax.swing.JLabel lblTitulo2;
    public javax.swing.JLabel lblUnidades;
    private javax.swing.JLabel lblcocina;
    public javax.swing.JTable tbBusquedaIngrediente;
    public javax.swing.JTable tbIngredientes;
    public javax.swing.JTextField txfBarraIngrediente;
    public javax.swing.JTextField txfCantidad;
    public javax.swing.JTextField txfCostoTotal;
    public javax.swing.JTextField txfDescripcion;
    public javax.swing.JTextField txfID;
    public javax.swing.JTextField txfRendimiento;
    // End of variables declaration//GEN-END:variables
}
