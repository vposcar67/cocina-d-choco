CREATE DATABASE  IF NOT EXISTS `cocina` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci */;
USE `cocina`;
-- MariaDB dump 10.19  Distrib 10.4.28-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: cocina
-- ------------------------------------------------------
-- Server version	10.4.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comandas`
--

DROP TABLE IF EXISTS `comandas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comandas` (
  `idVenta` varchar(6) NOT NULL,
  `idReceta` varchar(6) DEFAULT NULL,
  `idInsumo` varchar(6) DEFAULT NULL,
  `cantidad` float NOT NULL,
  `precio_cantidad` float NOT NULL,
  KEY `idVenta` (`idVenta`),
  KEY `idReceta` (`idReceta`),
  KEY `fk_Comandas_Inventario1` (`idInsumo`),
  CONSTRAINT `comandas_ibfk_1` FOREIGN KEY (`idVenta`) REFERENCES `ventas` (`idVenta`),
  CONSTRAINT `comandas_ibfk_2` FOREIGN KEY (`idReceta`) REFERENCES `recetas` (`idReceta`),
  CONSTRAINT `fk_Comandas_Inventario1` FOREIGN KEY (`idInsumo`) REFERENCES `inventario` (`idInsumo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comandas`
--

LOCK TABLES `comandas` WRITE;
/*!40000 ALTER TABLE `comandas` DISABLE KEYS */;
INSERT INTO `comandas` VALUES ('VEN001','REC001',NULL,3,0),('VEN001','REC002',NULL,2,0),('VEN002','REC003',NULL,2.5,0),('VEN002','REC004',NULL,1.5,0),('VEN003','REC001',NULL,4,0),('VEN003','REC002',NULL,3,0),('VEN003','REC005',NULL,2.5,0),('VEN004','REC003',NULL,2,0),('VEN004','REC004',NULL,1,0),('VEN005','REC002',NULL,2.5,0),('VEN005','REC004',NULL,1.5,0),('VEN005','REC005',NULL,3.5,0),('VEN006','REC001',NULL,3.5,0),('VEN006','REC003',NULL,2,0),('VEN006','REC005',NULL,1.5,0),('VEN007','REC004',NULL,2.5,0),('VEN007','REC005',NULL,1.5,0);
/*!40000 ALTER TABLE `comandas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleados`
--

DROP TABLE IF EXISTS `empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleados` (
  `idEmpleado` varchar(6) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `teléfono` varchar(10) NOT NULL,
  `domicilio` varchar(45) NOT NULL,
  `RFC` varchar(45) NOT NULL,
  `Estado` varchar(8) NOT NULL DEFAULT '1',
  `contrasena` varchar(10) DEFAULT '1234',
  `permisos` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`idEmpleado`),
  UNIQUE KEY `idEmpleado_UNIQUE` (`idEmpleado`),
  UNIQUE KEY `RFC_UNIQUE` (`RFC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleados`
--

LOCK TABLES `empleados` WRITE;
/*!40000 ALTER TABLE `empleados` DISABLE KEYS */;
INSERT INTO `empleados` VALUES ('EMP001','Oscar Axel Vega Piña','6691665367','calle 1ro de Octubre #524 Salinas de Gortari','aaaa','Activo','1234',1),('EMP002','Luis Antonio Romero ','6691578322','Monstruosa','bbbb','Activo','5678',2),('EMP003','Carlos David Rodrigu','6691587456','Pradera Dorada','HOMJ830910AB5','Activo','91011',3),('EMP004','Rosa Angélica Rosales Camacho','6691668459','Cerritos','XXXXXXXXXXXX','Inactivo','246810',1);
/*!40000 ALTER TABLE `empleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventario` (
  `idInsumo` varchar(6) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `existencia` float NOT NULL,
  `unidades` varchar(4) NOT NULL,
  `costo_unidad` float NOT NULL,
  `Estado` varchar(8) NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`idInsumo`),
  UNIQUE KEY `idInsumo_UNIQUE` (`idInsumo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario`
--

LOCK TABLES `inventario` WRITE;
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;
INSERT INTO `inventario` VALUES ('INS001','Pasta de espagueti',100,'KG',25,'Activo'),('INS002','Frijol',100,'KG',25,'Activo'),('INS003','Harina de maíz (para tortillas)',112,'KG',12,'Activo'),('INS004','Harina de trigo',106,'KG',20,'Activo'),('INS005','Huevo',104,'PZA',3.5,'Activo'),('INS006','Pollo (muslo)',102.25,'KG',50,'Activo'),('INS007','Carne de cerdo (chuleta)',102,'KG',63,'Activo'),('INS008','Carne de res (falda)',100,'KG',75,'Activo'),('INS009','Pescado (tilapia)',100,'KG',80,'Activo'),('INS010','Tomate',99.9625,'KG',15,'Activo'),('INS011','Cebolla',106,'KG',12,'Activo'),('INS012','Ajo',103,'KG',40,'Activo'),('INS013','Zanahoria',104.887,'KG',20,'Activo'),('INS014','Papa',102,'KG',18,'Activo'),('INS015','Chile fresco (jalapeño)',104,'KG',30,'Activo'),('INS016','Aguacate',115,'KG',45,'Activo'),('INS017','Limón',107.775,'KG',20,'Activo'),('INS018','Plátano',105,'KG',20,'Activo'),('INS019','Manzana',104,'KG',35,'Activo'),('INS020','Naranja',103,'KG',18,'Activo'),('INS021','Cilantro',100,'KG',15,'Activo'),('INS022','Perejil',100,'KG',25,'Activo'),('INS023','Calabacín',100,'KG',22,'Activo'),('INS024','Lechuga',98.875,'KG',15,'Activo'),('INS025','Espinaca',100,'KG',25,'Activo'),('INS026','Pepino',99.55,'KG',18,'Activo'),('INS027','Mango',100,'KG',35,'Activo'),('INS028','Piña',100,'KG',40,'Activo'),('INS029','Melón',100,'KG',25,'Activo'),('INS030','Sandía',100,'KG',12,'Activo'),('INS031','Fresa',100,'KG',40,'Activo'),('INS032','Queso panela',100,'KG',70,'Activo'),('INS033','Crema',100,'L',30,'Activo'),('INS034','Aceite vegetal',99.8491,'L',25,'Activo'),('INS035','Manteca',100,'KG',50,'Activo'),('INS036','Azúcar',100,'KG',10,'Activo'),('INS037','Sal',99.991,'KG',5,'Activo'),('INS038','Caldo de pollo (cubos)',99.625,'L',15,'Activo'),('INS039','Vinagre',100,'L',18,'Activo'),('INS040','Salsa de tomate',100,'L',25,'Activo'),('INS041','Salsa de soja',100,'L',30,'Activo'),('INS042','Salsa picante (salsa de chile)',100,'L',35,'Activo'),('INS043','Mostaza',100,'L',20,'Activo'),('INS044','Mayonesa',100,'L',22,'Activo'),('INS045','Café',100,'KG',65,'Activo'),('INS047','Chocolate en polvo',100,'KG',55,'Activo'),('INS048','Avena',100,'KG',25,'Activo'),('INS049','Canela en polvo',100,'KG',30,'Activo'),('INS050','Arroz',100,'kg',25,'Activo'),('INS051','Tomatillos',100,'kg',30,'Activo'),('INS052','Chile Verde',92,'KG',40,'Activo'),('INS053','Atún en Lata',97.75,'kg',28,'Activo'),('INS054','Carne de Res',100,'KG',120,'Activo'),('INS055','Salsa',100,'KG',35,'Activo'),('INS056','Pimienta',99.9977,'KG',50,'Activo'),('INS057','prueba 2',200,'L',200,'Activo');
/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lista_pedido`
--

DROP TABLE IF EXISTS `lista_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista_pedido` (
  `idPedido` varchar(6) NOT NULL,
  `idInsumo` varchar(6) NOT NULL,
  `cantidad` float NOT NULL,
  KEY `fk_Lista_Pedido_Pedidos` (`idPedido`),
  KEY `fk_Lista_Pedido_Inventario1` (`idInsumo`),
  CONSTRAINT `fk_Lista_Pedido_Inventario1` FOREIGN KEY (`idInsumo`) REFERENCES `inventario` (`idInsumo`),
  CONSTRAINT `fk_Lista_Pedido_Pedidos` FOREIGN KEY (`idPedido`) REFERENCES `pedidos` (`idPedido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista_pedido`
--

LOCK TABLES `lista_pedido` WRITE;
/*!40000 ALTER TABLE `lista_pedido` DISABLE KEYS */;
INSERT INTO `lista_pedido` VALUES ('PED001','INS001',10),('PED001','INS002',5),('PED001','INS001',10),('PED001','INS002',5),('PED001','INS003',8),('PED001','INS004',3),('PED001','INS005',2),('PED002','INS003',12),('PED002','INS004',6),('PED002','INS005',4),('PED002','INS006',3),('PED002','INS007',2),('PED003','INS011',6),('PED003','INS012',3),('PED003','INS013',5),('PED003','INS014',2),('PED003','INS015',4),('PED004','INS016',15),('PED004','INS017',8),('PED004','INS018',5),('PED004','INS019',4),('PED004','INS020',3),('PED005','INS021',7),('PED005','INS022',4),('PED005','INS023',6),('PED005','INS024',3),('PED005','INS025',2),('PED006','INS044',4),('PED006','INS054',10),('PED006','INS003',2);
/*!40000 ALTER TABLE `lista_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidos` (
  `idPedido` varchar(6) NOT NULL,
  `Proveedor` varchar(6) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `fecha_pedido` date NOT NULL,
  `Pedido_por` varchar(6) NOT NULL,
  `fecha_recibido` date DEFAULT NULL,
  `Recibido_por` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`idPedido`),
  UNIQUE KEY `idPedido_UNIQUE` (`idPedido`),
  KEY `fk_Pedidos_Proveedores1_idx` (`Proveedor`),
  KEY `fk_Pedidos_Empleados1_idx` (`Pedido_por`),
  KEY `fk_Pedidos_Empleados2_idx` (`Recibido_por`),
  CONSTRAINT `fk_Pedidos_Empleados1` FOREIGN KEY (`Pedido_por`) REFERENCES `empleados` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedidos_Empleados2` FOREIGN KEY (`Recibido_por`) REFERENCES `empleados` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedidos_Proveedores1` FOREIGN KEY (`Proveedor`) REFERENCES `proveedores` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedidos`
--

LOCK TABLES `pedidos` WRITE;
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT INTO `pedidos` VALUES ('PED001','PRO001','Cancelado','2023-07-26','EMP001',NULL,NULL),('PED002','PRO002','Recibido','2023-07-27','EMP002','2023-08-14','EMP002'),('PED003','PRO003','Recibido','2023-07-28','EMP003','2023-08-14','EMP001'),('PED004','PRO002','Recibido','2023-07-29','EMP002','2023-08-14','EMP001'),('PED005','PRO001','En proceso','2023-07-30','EMP003',NULL,NULL),('PED006','PRO002','Cancelado','2023-08-14','EMP001',NULL,NULL);
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preparacion`
--

DROP TABLE IF EXISTS `preparacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preparacion` (
  `idOrden` varchar(6) NOT NULL,
  `idReceta` varchar(6) NOT NULL,
  `cantidad` float NOT NULL,
  `Preparado_por` varchar(6) NOT NULL,
  `fecha_orden` date NOT NULL,
  PRIMARY KEY (`idOrden`),
  UNIQUE KEY `idOrden_UNIQUE` (`idOrden`),
  KEY `fk_Preparacion_Recetas1_idx` (`idReceta`),
  KEY `fk_Preparacion_Empleados1_idx` (`Preparado_por`),
  CONSTRAINT `fk_Preparacion_Empleados1` FOREIGN KEY (`Preparado_por`) REFERENCES `empleados` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Preparacion_Recetas1` FOREIGN KEY (`idReceta`) REFERENCES `recetas` (`idReceta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preparacion`
--

LOCK TABLES `preparacion` WRITE;
/*!40000 ALTER TABLE `preparacion` DISABLE KEYS */;
INSERT INTO `preparacion` VALUES ('ORD001','REC001',5,'EMP001','2023-07-25'),('ORD002','REC002',3,'EMP002','2023-07-25'),('ORD003','REC003',2.5,'EMP003','2023-07-25'),('ORD004','REC004',4,'EMP001','2023-07-26'),('ORD005','REC001',6,'EMP002','2023-07-26'),('ORD006','REC005',3.5,'EMP003','2023-07-26'),('ORD007','REC002',4,'EMP001','2023-07-27'),('ORD008','REC003',2,'EMP002','2023-07-27'),('ORD009','REC004',3.5,'EMP003','2023-07-27'),('ORD010','REC005',5,'EMP001','2023-07-28'),('ORD011','REC003',3,'EMP002','2023-08-12'),('ORD012','REC004',2,'EMP001','2023-08-13'),('ORD013','REC004',3,'EMP001','2023-08-13'),('ORD014','REC001',4,'EMP002','2023-08-14'),('ORD015','REC002',3,'EMP002','2023-08-15'),('ORD016','REC004',5,'EMP002','2023-08-15'),('ORD017','REC004',4,'EMP002','2023-08-15');
/*!40000 ALTER TABLE `preparacion` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `orden_inventario` AFTER INSERT ON `preparacion` FOR EACH ROW BEGIN
    -- Obtenemos los insumos de la receta
    DECLARE insumo_id VARCHAR(6);
    DECLARE insumo_cantidad FLOAT;
    DECLARE done INT DEFAULT 0;
    DECLARE cur CURSOR FOR
        SELECT RI.idInsumo, RI.cantidad * NEW.cantidad / recetas.rendimiento_preparacion
        FROM Recetas_Insumos RI INNER JOIN recetas ON recetas.idReceta = NEW.idReceta
        WHERE RI.idReceta = NEW.idReceta;

    -- Iteramos por los insumos y actualizamos el inventario
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1; 

    UPDATE recetas rec SET rec.Existencia = rec.Existencia + NEW.cantidad WHERE rec.idReceta = NEW.idReceta;

    OPEN cur;
    read_loop: LOOP
        FETCH cur INTO insumo_id, insumo_cantidad;
        IF done THEN
            LEAVE read_loop;
        END IF;

        -- Actualizamos la existencia en el inventario
        UPDATE Inventario
        SET existencia = existencia - insumo_cantidad
        WHERE idInsumo = insumo_id;
    END LOOP;
    CLOSE cur;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `idProveedor` varchar(6) NOT NULL,
  `empresa` varchar(45) NOT NULL,
  `teléfono` varchar(10) NOT NULL,
  `domicilio` varchar(45) DEFAULT NULL,
  `horario` varchar(45) DEFAULT NULL,
  `resumen_catalogo` varchar(100) NOT NULL,
  `Estado` varchar(8) NOT NULL,
  PRIMARY KEY (`idProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES ('PRO001','Distribuidora Alimentos SA','555-1234','Av. Juárez #123','9:00 AM - 5:00 PM','Productos alimenticios','Activo'),('PRO002','Proveedores Express','555-5678','Calle Reforma #456','8:00 AM - 6:00 PM','Variedad de insumos','Activo'),('PRO003','Frutas y Verduras Frescas','555-9876','Blvd. Hidalgo #789','7:00 AM - 4:00 PM','Frutas y verduras','Activo'),('PRO004','Carnicería San Juan','555-2468','Calle Madero #135','10:00 AM - 7:00 PM','Carnes frescas y embutidos','Activo'),('PRO005','Panadería El Buen Sabor','555-7890','Av. Revolución #789','6:00 AM - 3:00 PM','Pan dulce y repostería','Inactivo');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recetas`
--

DROP TABLE IF EXISTS `recetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recetas` (
  `idReceta` varchar(6) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `rendimiento_preparacion` float NOT NULL,
  `costo_preparacion` float DEFAULT NULL,
  `Existencia` float DEFAULT NULL,
  `Estado` varchar(8) NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`idReceta`),
  UNIQUE KEY `idReceta_UNIQUE` (`idReceta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recetas`
--

LOCK TABLES `recetas` WRITE;
/*!40000 ALTER TABLE `recetas` DISABLE KEYS */;
INSERT INTO `recetas` VALUES ('REC001','Arroz a la Mexicana',4,37.4,4,'Activo'),('REC002','Pollo en Salsa Verde',4,63.28,3,'Activo'),('REC003','Spaghetti con Salsa de Tomate',4,42.68,0,'Activo'),('REC004','Ensalada de Atún',4,42.17,14,'Activo'),('REC005','Tacos de Carne Asada',4,93.44,0,'Activo'),('REC006','ÑOQUIS',2,10.05,0,'Inactivo');
/*!40000 ALTER TABLE `recetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recetas_insumos`
--

DROP TABLE IF EXISTS `recetas_insumos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recetas_insumos` (
  `idReceta` varchar(6) NOT NULL,
  `idInsumo` varchar(6) NOT NULL,
  `cantidad` float NOT NULL,
  KEY `idReceta` (`idReceta`),
  KEY `idInsumo` (`idInsumo`),
  CONSTRAINT `recetas_insumos_ibfk_1` FOREIGN KEY (`idReceta`) REFERENCES `recetas` (`idReceta`),
  CONSTRAINT `recetas_insumos_ibfk_2` FOREIGN KEY (`idInsumo`) REFERENCES `inventario` (`idInsumo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recetas_insumos`
--

LOCK TABLES `recetas_insumos` WRITE;
/*!40000 ALTER TABLE `recetas_insumos` DISABLE KEYS */;
INSERT INTO `recetas_insumos` VALUES ('REC006','INS014',0.1),('REC006','INS006',0.15),('REC006','INS010',0.05),('REC004','INS053',1),('REC004','INS024',0.5),('REC004','INS026',0.2),('REC004','INS013',0.05),('REC004','INS017',0.1),('REC004','INS037',0.004),('REC004','INS056',0.001),('REC003','INS001',1),('REC003','INS011',0.5),('REC003','INS012',0.2),('REC003','INS013',0.05),('REC003','INS034',0.1),('REC003','INS037',0.006),('REC003','INS056',0.003),('REC002','INS006',1),('REC002','INS038',0.5),('REC002','INS034',0.2),('REC002','INS010',0.05),('REC002','INS034',0.0012),('REC001','INS050',1),('REC001','INS010',0.5),('REC001','INS011',0.2),('REC001','INS034',0.1),('REC005','INS008',1),('REC005','INS021',0.5),('REC005','INS011',0.2),('REC005','INS024',0.1),('REC005','INS055',0.2),('REC005','INS037',0.008);
/*!40000 ALTER TABLE `recetas_insumos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `idVenta` varchar(6) NOT NULL,
  `Cliente` varchar(30) NOT NULL,
  `fecha_venta` date NOT NULL,
  `precio_total` float NOT NULL,
  PRIMARY KEY (`idVenta`),
  UNIQUE KEY `idVenta_UNIQUE` (`idVenta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES ('VEN001','Juan Perez','2023-07-25',150),('VEN002','María Gómez','2023-07-26',180),('VEN003','Carlos López','2023-07-27',200),('VEN004','Laura Torres','2023-07-28',120),('VEN005','Pedro Ramirez','2023-07-29',250),('VEN006','Ana Rodriguez','2023-07-30',190),('VEN007','José Martínez','2023-07-31',170);
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cocina'
--

--
-- Dumping routines for database 'cocina'
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `nextEMP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextEMP`() RETURNS varchar(6) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
    DECLARE siguiente_id VARCHAR(6);

    SELECT idEmpleado INTO siguiente_id
    FROM empleados
    ORDER BY idEmpleado DESC
    LIMIT 1;

    IF siguiente_id IS NULL THEN
        SET siguiente_id = 'EMP001';
    ELSE
        SET siguiente_id = SUBSTRING(siguiente_id, 4);
        SET siguiente_id = siguiente_id + 1;

        SET siguiente_id = LPAD(siguiente_id, 3, '0');

        SET siguiente_id = CONCAT('EMP', siguiente_id);
    END IF;

    RETURN siguiente_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `nextINS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextINS`() RETURNS varchar(6) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
    DECLARE siguiente_id VARCHAR(6);

    SELECT idInsumo INTO siguiente_id
    FROM Inventario
    ORDER BY idInsumo DESC
    LIMIT 1;

    IF siguiente_id IS NULL THEN
        SET siguiente_id = 'INS001';
    ELSE
        SET siguiente_id = SUBSTRING(siguiente_id, 4);
        SET siguiente_id = siguiente_id + 1;

        SET siguiente_id = LPAD(siguiente_id, 3, '0');

        SET siguiente_id = CONCAT('INS', siguiente_id);
    END IF;

    RETURN siguiente_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `nextORD` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextORD`() RETURNS varchar(6) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
    DECLARE siguiente_id VARCHAR(6);

    SELECT idOrden INTO siguiente_id
    FROM preparacion
    ORDER BY idOrden DESC
    LIMIT 1;

    IF siguiente_id IS NULL THEN
        SET siguiente_id = 'ORD001';
    ELSE
        SET siguiente_id = SUBSTRING(siguiente_id, 4);
        SET siguiente_id = siguiente_id + 1;

        SET siguiente_id = LPAD(siguiente_id, 3, '0');

        SET siguiente_id = CONCAT('ORD', siguiente_id);
    END IF;

    RETURN siguiente_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `nextPED` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextPED`() RETURNS varchar(6) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
    DECLARE siguiente_id VARCHAR(6);

    SELECT idPedido INTO siguiente_id
    FROM pedidos
    ORDER BY idPedido DESC
    LIMIT 1;

    IF siguiente_id IS NULL THEN
        SET siguiente_id = 'PED001';
    ELSE
        SET siguiente_id = SUBSTRING(siguiente_id, 4);
        SET siguiente_id = siguiente_id + 1;

        SET siguiente_id = LPAD(siguiente_id, 3, '0');

        SET siguiente_id = CONCAT('PED', siguiente_id);
    END IF;

    RETURN siguiente_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `nextPRO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextPRO`() RETURNS varchar(6) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
    DECLARE siguiente_id VARCHAR(6);

    SELECT idProveedor INTO siguiente_id
    FROM proveedores
    ORDER BY idProveedor DESC
    LIMIT 1;

    IF siguiente_id IS NULL THEN
        SET siguiente_id = 'PRO001';
    ELSE
        SET siguiente_id = SUBSTRING(siguiente_id, 4);
        SET siguiente_id = siguiente_id + 1;

        SET siguiente_id = LPAD(siguiente_id, 3, '0');

        SET siguiente_id = CONCAT('PRO', siguiente_id);
    END IF;

    RETURN siguiente_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `nextREC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextREC`() RETURNS varchar(6) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
    DECLARE siguiente_id VARCHAR(6);

    SELECT idReceta INTO siguiente_id
    FROM recetas
    ORDER BY idReceta DESC
    LIMIT 1;

    IF siguiente_id IS NULL THEN
        SET siguiente_id = 'REC001';
    ELSE
        SET siguiente_id = SUBSTRING(siguiente_id, 4);
        SET siguiente_id = siguiente_id + 1;

        SET siguiente_id = LPAD(siguiente_id, 3, '0');

        SET siguiente_id = CONCAT('REC', siguiente_id);
    END IF;

    RETURN siguiente_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP FUNCTION IF EXISTS `nextVEN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextVEN`() RETURNS varchar(6) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
    DECLARE siguiente_id VARCHAR(6);

    SELECT idVenta INTO siguiente_id
    FROM ventas
    ORDER BY idVenta DESC
    LIMIT 1;

    IF siguiente_id IS NULL THEN
        SET siguiente_id = 'VEN001';
    ELSE
        SET siguiente_id = SUBSTRING(siguiente_id, 4);
        SET siguiente_id = siguiente_id + 1;

        SET siguiente_id = LPAD(siguiente_id, 3, '0');

        SET siguiente_id = CONCAT('VEN', siguiente_id);
    END IF;

    RETURN siguiente_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ActualizarInventarioPedido` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarInventarioPedido`(IN pedidoId VARCHAR(6))
BEGIN
        -- Actualizar el inventario
        DECLARE insumo_id VARCHAR(6);
        DECLARE insumo_cantidad FLOAT;
        DECLARE done INT DEFAULT 0;
        DECLARE cur CURSOR FOR
            SELECT idInsumo, cantidad
            FROM Lista_Pedido
            WHERE idPedido = pedidoId;

        -- Iterar por los insumos y actualizar el inventario
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        OPEN cur;
        read_loop: LOOP
            FETCH cur INTO insumo_id, insumo_cantidad;
            IF done THEN
                LEAVE read_loop;
            END IF;

            -- Actualizar la existencia en el inventario
            UPDATE Inventario
            SET existencia = existencia + insumo_cantidad
            WHERE idInsumo = insumo_id;
        END LOOP;
        CLOSE cur;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `añadirEmpleado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`oscar`@`localhost` PROCEDURE `añadirEmpleado`(IN `nom` VARCHAR(50), IN `tel` VARCHAR(10), IN `domi` VARCHAR(50), IN `codRFC` VARCHAR(45), IN `contrasena` VARCHAR(20))
    SQL SECURITY INVOKER
BEGIN
	INSERT INTO empleados VALUES(nextEMP(),nom,tel,domi,codRFC,'Activo',contrasena,1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `añadirInsumo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `añadirInsumo`(IN `descripcion` VARCHAR(50), IN `existencia` FLOAT UNSIGNED, IN `unidades` VARCHAR(3), IN `costo` FLOAT UNSIGNED)
BEGIN
	insert into inventario values (nextINS(),descripcion,existencia,unidades,costo,'Activo');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `añadirPedidoInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`CHOCO`@`%` PROCEDURE `añadirPedidoInfo`(
    IN proveedor_id VARCHAR(6),
    IN empleado_id VARCHAR(6)
)
BEGIN
    INSERT INTO pedidos (idPedido, Proveedor, estado, fecha_pedido, Pedido_por)
    VALUES (nextPED(), proveedor_id, 'En proceso', CURDATE(), empleado_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `añadirPedidoInsumos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`CHOCO`@`%` PROCEDURE `añadirPedidoInsumos`(IN `idPedido` VARCHAR(6), IN `idInsumo` VARCHAR(6), IN `cantidad` FLOAT)
BEGIN
    INSERT INTO lista_pedido
    VALUES (idPedido, idInsumo, cantidad);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `añadirPreparacion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `añadirPreparacion`(IN `idReceta` VARCHAR(6), IN `cantidad` FLOAT, IN `idEmpleado` VARCHAR(6))
INSERT INTO preparacion VALUES(nextORD(),idReceta,cantidad,idEmpleado,CURRENT_DATE()) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `añadirProveedor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `añadirProveedor`(IN `empresa` VARCHAR(100), IN `telefono` VARCHAR(10), IN `direccion` VARCHAR(100), IN `horario` VARCHAR(20), IN `catalogo` VARCHAR(100))
INSERT INTO proveedores VALUES(nextPRO(),empresa,telefono,direccion,horario,catalogo,'Activo') ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `añadirRecetaInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `añadirRecetaInfo`(IN `descripcion` VARCHAR(50), IN `rendimiento` FLOAT, IN `costo` FLOAT)
BEGIN
	INSERT INTO recetas VALUES (nextREC(),descripcion,rendimiento,costo,0,'Activo');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `añadirRecetaIngredientes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `añadirRecetaIngredientes`(IN `Receta` VARCHAR(6), IN `Insumo` VARCHAR(6), IN `cantidad` FLOAT)
BEGIN
	INSERT INTO recetas_insumos VALUES (Receta,Insumo,cantidad);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `consultaEmpleados` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultaEmpleados`()
BEGIN
SELECT
    idEmpleado as ID,
    nombre as 'Nombre completo',
    teléfono as Teléfono, 
    domicilio as Domicilio,
    RFC, contrasena as Contraseña,
    Estado
FROM
    empleados
ORDER BY
	Estado;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `consultaPedidos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultaPedidos`()
BEGIN	
SELECT 
    p.idPedido as ID,
    pr.empresa as Proveedor,
    p.fecha_pedido as 'Fecha del pedido',
    p.estado as Estado,
    ep.nombre as 'Pedido por',
    p.fecha_recibido as 'Fecha de recibido',
    er.nombre as 'Recibido por'
FROM 
    pedidos p
LEFT JOIN 
    proveedores pr ON p.Proveedor = pr.idProveedor
LEFT JOIN 
    empleados ep ON p.Pedido_por = ep.idEmpleado
LEFT JOIN 
    empleados er ON p.Recibido_por = er.idEmpleado
ORDER BY
	FIELD(p.estado, 'En proceso', 'Recibido','Cancelado'), p.fecha_pedido DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `consultaPreparaciones` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultaPreparaciones`()
SELECT
    pre.idOrden as ID,
    rec.descripcion as Receta,
    pre.cantidad as Cantidad,
    emp.nombre as 'Preparado por',
    pre.fecha_orden as 'Fecha de la preparación'
FROM 
    preparacion pre
LEFT JOIN
    recetas rec ON pre.idReceta = rec.idReceta
LEFT JOIN
    empleados emp ON emp.idEmpleado = pre.Preparado_por
ORDER BY
	pre.fecha_orden desc ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `consultaProveedores` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultaProveedores`()
BEGIN
SELECT
    idProveedor as ID,
    empresa as Proveedor,
    resumen_catalogo as 'Tipos de productos',
    teléfono as Teléfono,
    domicilio as Domicilio,
    horario as Horario,
	Estado
FROM
    proveedores
ORDER BY
	Estado;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `consultaR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultaR`()
BEGIN
	SELECT idInsumo as ID, descripcion as Descripción, CONCAT(existencia," ",unidades) as Existencia, CONCAT('$',costo_unidad) as "Costo por unidad", Estado from inventario
ORDER BY Estado, idInsumo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `consultaRecetas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultaRecetas`()
BEGIN
SELECT
    idReceta as ID, descripcion as Descripción, CONCAT(rendimiento_preparacion," KG") as Rendimiento, CONCAT('$',costo_preparacion) as 'Costo por preparación', CONCAT(Existencia," KG") as 'Cantidad en existencia', Estado
FROM
    recetas
ORDER BY
	Estado;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `consultaVentas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `consultaVentas`()
BEGIN
SELECT
    idVenta as ID, Cliente, fecha_venta as 'Fecha de la venta', CONCAT('$',precio_total) as 'Precio total'
FROM
    ventas;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contVenta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`carlos`@`%` PROCEDURE `contVenta`(IN `fecha` DATE, OUT `total` INT)
BEGIN
	select count(*) into total from Ventas where fecha_venta=fecha;
	select @total;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ingresar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ingresar`(IN `ID` VARCHAR(6), IN `contra` VARCHAR(10))
BEGIN
SELECT COUNT(idEmpleado) from empleados where ID = empleados.idEmpleado AND contra = empleados.contrasena AND empleados.Estado = "Activo";
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `modeloIngredientesReceta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `modeloIngredientesReceta`(IN `ID` VARCHAR(6))
SELECT 
	ri.idInsumo as ID, 
	inv.descripcion as 'Ingrediente', 
	ri.cantidad as Cantidad, 
	ri.cantidad*inv.costo_unidad as Costo
FROM
	recetas_insumos ri
INNER JOIN
	inventario inv ON inv.idInsumo = ri.idInsumo
WHERE
	ri.idReceta = ID ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `modeloListaPedido` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `modeloListaPedido`(IN `ID` VARCHAR(6))
SELECT 
	ri.idInsumo as ID, 
	inv.descripcion as 'Ingrediente', 
	ri.cantidad as Cantidad, 
	ri.cantidad*inv.costo_unidad as Costo
FROM
	lista_pedido ri
INNER JOIN
	inventario inv ON inv.idInsumo = ri.idInsumo
WHERE
	ri.idPedido = ID ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `PA1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`carlos`@`%` PROCEDURE `PA1`(IN f1 DATE, IN f2 DATE, OUT f3 FLOAT, OUT f4 DATE)
BEGIN
	select * from Ventas where fecha_venta BETWEEN f1 AND f2;
	select SUM(precio_total) into f3 from Ventas where fecha_venta BETWEEN f1 AND f2;
	select fecha_venta into f4 from Ventas group by fecha_venta order by precio_total desc limit 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `PA2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`carlos`@`%` PROCEDURE `PA2`(IN f1 DATE, IN f2 DATE)
BEGIN
	select r.descripcion, sum(c.precio_cantidad) as Total
	from
		comandas c
		join recetas r on r.idReceta = c.idReceta
		join Ventas v on c.idVenta = v.idVenta
	where fecha_venta BETWEEN f1 AND f2
group by
    c.idReceta
order by
     sum( c.idReceta) desc limit 1;	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updExistencia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updExistencia`(
    IN codigo_producto VARCHAR(10),
    IN cantidad_sumar FLOAT
)
BEGIN
    DECLARE existencia_actual FLOAT;

    -- Obtener la existencia actual del producto
    SELECT existencia INTO existencia_actual
    FROM Inventario
    WHERE idInsumo = codigo_producto;

    IF existencia_actual IS NOT NULL THEN
        -- Actualizar la existencia sumando la cantidad indicada
        UPDATE Inventario
        SET existencia = existencia + cantidad_sumar
        WHERE idInsumo = codigo_producto;
    ELSE
        SELECT 'El código del producto no existe en el inventario.' AS mensaje;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updPrecio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`carlos`@`%` PROCEDURE `updPrecio`(IN f1 varchar(6), IN f2 FLOAT)
BEGIN
	UPDATE inventario
	SET costo_unidad=f2
	WHERE idinsumo = f1;
	select * from inventario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-08-15  1:23:23
